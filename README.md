# Ser Scout

Este proyecto inicio como la necesidad de centralizar y filtrar los contenidos que se encuentran dispersos por todo el mundo. El escultismo se practica en todo el mundo y cada lugar tiene su adaptación. Sin embargo, muchas adaptaciones pueden perder la esencia del escultismo. De esta forma se decide generar un sitio donde todos los scouts del mundo (Actualmente de habla hispana) puedan contribuir y buscar información de confianza.


## ¿Como contribuir?

Es real que esta estructura de Blog a diferencia de otros no es tan simple de editar ni tan fácil de redactar, sin embargo, se eligió esta tecnología con el fin de representar la libertad del escultismo. La contribución de los usuarios que deseen participar se realizara vía email con el fin de que el autor no tenga que preocuparse por mas nada que su publicación y nosotros hacemos el resto


## Rumbo del Proyecto

Creemos firmemente que los scouts deben afianzarse con la tecnología, tecnología que los representen y tengan los mimos ideales, (por ejemplo, el software libre) y nosotros pretendemos brindar herramientas libres para que los mismos utilizan como complemento a su educación scout en el caso de disponer de la posibilidad de uso.
