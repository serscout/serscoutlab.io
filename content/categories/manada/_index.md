---
title: 'Manada'
description: 'Niños entre 7 y 11 años'
type: 'category'
color: "manada"
img: 'https://i.postimg.cc/Y0m5k7Qn/lobato.jpg'
---

La manada es, en la mayoria de los grupos, la primer rama, la de los niños mas pequeños (entre 7 y 11 años). Fomenta el inicio en el Juego Scout y el concepto basico de las tecnicas.
