---
name: "Joaquin Decima"
country: "Argentina"
description: "Educador Rover"
photo: 'https://i.postimg.cc/Pq1zntrx/10271648-556446921138221-2502803513813530315-n.jpg'
---

Inicie en el Movimiento Scout a la corta edad de 7 años en el grupo G.S. Ing. Naval Armando Fischer de Quilmes, Buenos Aires, Argentina. A los 17 Años me aleje de ese grupo y de Scout de Argentina en general por diferir con sus ideas de "los scouts son empresarios" y sus uniformes poco útiles para el Movimiento. Continúe mi formación y me convertí dirigente Raider a la corta edad de 18 años. Luego de colaborar con diversos grupos en el 2019 inicie mi actividad en el G.S. Indios Kilmes de Quilmes, Buenos Aires, Argentina como dirigente Raider.
