---
name: "Norberto Daniel Ceballos"
country: "Argentina"
description: "Dirigente Caminante"
photo: 'https://i.postimg.cc/Bvm8p8SY/image.png'
---

Mi Nombre es Norberto Daniel Ceballos, ingresé al movimiento en el año 2010, en la rama Caminante del Grupo Scout Ing. Naval A. Fischer y realicé toda la etapa Caminante y Rover en el Grupo. Actualmente me desempeño como Educador en la rama Caminante hace 3 años, entregando toda la pasión y el amor al movimiento y sus enseñanzas.
