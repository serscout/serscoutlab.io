---
title: 'Campaña de donación'
date: '2021-06-16 12:36:00'
description: 'Donar sangre es un compromiso muy importante el cual todos debemos asumir'
type: 'publicaciones'
categories: ["raiders","tropa","rovers","manada"]
color: "rover"
theme: 'opiniones'
tags: ["donar", "donacion", "sangre", "rovers", "rover", "campaña", "medula", "proyecto"]
img: 'https://med.unne.edu.ar/notimed/wp-content/uploads/2019/11/Screenshot_2019-06-11-13-58-32-824_com.android.chrome.jpg'
authors: ["Joaquin Decima"]
---

Los rovers del grupo scout Indios Kilmes, de Scout de Argentina. Están con un proyecto, sobre el cual nos hablarán más adelante. Sin embargo, como parte de ese gran proyecto comenzaron una campaña sobre la concientización sobre la donación de sangre.

Antes que nada quería agradecer enormemente al Grupo Scout Indios Kilmes por acercarnos todo el trabajo realizado por el Clan:


{{< youtube code="bRoM3l783tM" >}}


¿Vos qué pensas?¿Sos donador?¿Nos queres ayudar a llevar este mensaje a todo el mundo?

Próximamente vamos a estar hablando con los chicos del grupo para que nos cuenten más de su proyecto, que según nos adelantaron, va más allá de la donación de sangre.
