---
title: 'Baden Powell'
date: '2019-06-24'
description: 'Un minimo de informacion sobre nuestro gran y querido Fundador B.P.'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "historias"
tags: ["fundador", "bp", "historias"]
img: 'https://cadenaser00.epimg.net/emisora/imagenes/2017/02/22/radio_murcia/1487771456_401426_1487772141_noticia_normal.jpg'
authors: ["Joaquin Decima"]
---

{{< img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7hoGbwJGq-pxcxN4AQG7SSd_o0dikJj9t9LcofhX65WJFNGHD" >}}

Robert Stephenson Smyth Baden-Powell, I barón de Gilwell  fue un pintor, músico, militar, escultor y escritor británico. Fundador del Movimiento Scout Mundial, participó en distintas campañas militares en África, en las cuales destacó y obtuvo gran popularidad entre la población británica, especialmente por su heroica dirección en la defensa de Mafeking. Tras regresar a su isla natal, las publicaciones de sus libros se multiplicaron y se convirtió, así, en un destacado autor en materia de educación y formación juvenil. Sus ideas, plasmadas en Escultismo para muchachos y otras obras, inspiraron a grupos de jóvenes británicos a formar patrullas, con lo que se inició de manera informal el escultismo.

{{< img src="https://voiceofscouting.org/wp-content/uploads/2017/04/Beard1.jpg" >}}



Tras su renuncia al ejército británico por consejo del rey Eduardo VII, se dedicó plenamente a la formación del movimiento scout. Participó en actividades por todo el mundo, propagando y asentando las bases del escultismo moderno. Escribió una gran cantidad de libros adecuados a las necesidades del movimiento y a los diferentes niveles de participación de los scouts, permitiendo que estos, en todo el mundo, tuviesen la oportunidad de aprender mediante textos especializados para su edad. Tras obtener diversos premios y reconocimientos, se retiró con su esposa a Kenia, donde murió. Fue sepultado en Nyeri; su tumba tiene inscrito un mensaje de Baden-Powell para todos los scouts del mundo. "Intentad dejar este mundo mejor de como os lo encontrasteis" que resume uno de los fines a lograr en los jóvenes a través del Escultismo.
