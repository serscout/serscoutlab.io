---
title: 'Intolerancia discriminativa dentro del Escultismo'
date: '2019-06-30'
description: 'Esta publicación es una opinión sobre el tema que está abatiendo estos días las redes en y el escultismo'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "opiniones"
tags: ["escultismo", "discriminacion", "opiniones"]
img: 'https://scontent.faep9-1.fna.fbcdn.net/v/t1.0-9/64785961_502039667204062_1998556550364397568_n.jpg?_nc_cat=100&_nc_oc=AQk70k7a2PE7lzkyJhpEfH08ZGE2HuTdnF-NmRLzKVkJeHdDe0ThYtAVroY31zCATxc&_nc_ht=scontent.faep9-1.fna&oh=21664e97017f2fa8327d94f70de316c7&oe=5DC5CA08'
authors: ["Joaquin Decima"]
---

En estos días se desato un “debate” sobre el tema homosexualidad y escultismo. Antes de leer el resto de la publicación quiero recordar que esto es un tema aparte del destino del blog. Esto lo escribo personalmente y cada persona que quiera escribir algo u opinar el blog está a su disposición.



## La flor de Liz

{{< img src="https://scontent.faep9-1.fna.fbcdn.net/v/t1.0-9/64785961_502039667204062_1998556550364397568_n.jpg?_nc_cat=100&_nc_oc=AQk70k7a2PE7lzkyJhpEfH08ZGE2HuTdnF-NmRLzKVkJeHdDe0ThYtAVroY31zCATxc&_nc_ht=scontent.faep9-1.fna&oh=21664e97017f2fa8327d94f70de316c7&oe=5DC5CA08" >}}

Antes que nada, quiero decir que no estoy de acuerdo con agregar colores a la flor de Liz. ¿Por qué? La flor de Liz nunca necesito colores para incluir a alguien. Siempre todos fueron bienvenidos y esto mismo lo comenta otro Scout al cual cito:

{{< cita text="Traigo mi comentario de esa publicación - No estoy de acuerdo con este logo. porque la naturaleza del scout no discrimina. los scouts nunca necesitamos una bandera para enseñar la inclusión. nos basta con nuestra ley, nuestros principios y nuestros valores. protegiendo al más débil como lo muestra nuestro saludo día a día. las condiciones sexuales de un scout no suman ni restan, de hecho, ni importa, un scout es importante por lo que hace con los demás, su comunidad, su familia, y con el mismo. lo demás es ideología que no entra en el escautismo. saludos y siempre listos!" name="Cristian Araniva" >}}


## BP y el escultismo

En esta parte puedo llegar a ignorar algún tema, y me gustaría que de ser así me lo hagan saber.
Entiendo que BP nunca le pregunto a las primeras patrullas que sexo amaban. Y en ninguna lectura encuentro nada que condicione una elección sexual. Por lo cual en el escultismo no existe ningún tipo de limitación y quien diga lo contrario, pido por favor que me ilumine con las escrituras que lo abalan.

{{< cita text="4to Artículo Scout: El scout es amigo de todos sin importar credo, clase social." name="Gabo Casillas" >}}


>>Un scout es un amigo para todos y un hermano para todos los demás scouts sin distinción de país, clase o credo a que pertenezcan



## La religión

En este punto pido que todo fanático de Dios, o la religión se abstengan de seguir leyendo dado que la crítica es dura y no quiero tildes después de esto.
Como algunos sabrán yo no soy católico, aunque lo fui parte de mi vida. El tiempo me alejo, si esto resulta interesante puedo en algún momento contar que paso.

{{< cita text="Primer principio: el scout se honra en su fe y le somete su vida... La ley de Dios no agrada la homosexualidad" name="Anahi Santos" >}}


Es increíble ver gente justificándolo conque “Dios no lo acepta” y ahora pido a todos y cada uno de ellos a citarme en que parte de su libro sagrado dice “Dios repudio a las personas que aman” o “Dios repudia a los homosexuales”. Esto es completamente absurdo contemplando que dios nos pide amar y encontrar a dios en el prójimo. Es imposible que un cristiano discrimine a una persona porque esa persona elije amar a otro del mismo sexo, no solo esto vi, también hay gente que dice que porque una persona ame a otro del mismo sexo este es un pedófilo. ¿Bajo qué concepto podemos decir eso? Si es más lógico que un Cura sea pedófilo, porque de esos ya vimos muchos. Las iglesias son la representación del anticristo en la tierra, usan el nombre de Dios para ir a cruzadas y cometer genocidios, para tomar partido en los estados y controlar temas como la educación dejando al súbdito ignorante. Reprimiendo todo aquello que su cúpula no acepte.
¿A caso creen que dios abalaría el negocio a base de genocidio y pederastia que genero la iglesia católica romana? ¿E incluso otras iglesias que piden un diezmo para comprar el paraíso? Entiendan que si Dios no fuera ausencia como lo es hoy en día el vaticano seria destruido para alimentar pobres. Y la iglesia usaría el poder para promulgar la paz y no esconder curas que violan y abusan de criaturas.

Todo aquel que meta Dios debería entender que dios pedía amor, no discriminación, la discriminación la genero la iglesia como parte de su negocio.



## ¿Enfermedad?

{{< cita text="Están usando muy mal esa ley scout los gay es una enfermedad y no se le debe transmitir a los niños los adultos enfermos deben ser retirados de los scouts o arruinaran el escultismo gravemente y la destrucción de las familias desde abajo PROTEJAN A LOS NIÑOS DE LAS ENFERMEDADES MENTALES Y EMOCIONALES QUE TIENE ESOS GAY NO MAS APOYO Y CAMPALA A ESOS ENFERMOS ... NO GAY EN LOS SCOUTS POR DIOS" name="Jose Tomas Vargas" >}}


Por ultimo quiero aclarar una cosa, vi gente que dice que ser gay es una enfermedad, a estas personas les pido que al igual que durante todo el post que me informen en que libro de medicina figura esto como una enfermad. De caso que esto no exista pido por favor que se hagan tratar, la homofobia si es una enfermedad y pueden verla con su psicólogo de confianza.




{{< cita text="Los respeto incluso tengo amigos, pero no comparto ni apoyo y también debe respetarse que yo no comparta ni apoye. ✌️" name="Torre victoria" >}}


Concuerdo con que debemos respetar a quienes no estén de acuerdo, sin embargo un scout no se queda callado ante un atropello y todo tipo de discriminación nosotros tenemos que actuar.

Todo esto nace a partir de la {{< textlink text="publicacion de Facebook" url="https://www.facebook.com/groups/1171749626314589/permalink/1240809182741966/" >}} y cualquiera puede ingresar a verlo
