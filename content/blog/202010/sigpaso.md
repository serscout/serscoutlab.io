---
title: '¿Cuál es el siguiente paso?'
date: '2020-10-01 15:03:00'
description: 'Llega una etapa del año en la cual, sábado a sábado, vamos pensando que actividades hacer para los scouts en el patio de juegos'
type: 'publicaciones'
categories: ["raiders","tropa","rovers","manada"]
color: "universal"
theme: 'opiniones'
tags: ["campamento", "manada", "tropa", "unidad", "caminantes", "raiders", "opiniones", "olimpiadas", "desarrollar", "intereses", "personales"]
img: 'https://i.postimg.cc/5yvqDX5f/image.png'
authors: ["Norberto Daniel Ceballos"]
---


{{< img src="https://i.postimg.cc/5yvqDX5f/image.png" >}}


Llega una etapa del año en la cual, sábado a sábado, vamos pensando que actividades hacer para los scouts en el patio de juegos, pero nos vamos quedando sin ideas que no sean repetitivas o muy tediosas. Si a esta situación le sumamos que en algunos países que nos leen, todavía estamos en este aislamiento social o cuarentena, que las reuniones sociales todavía no se pueden realizar y la tecnología nos esta limitando, se nos viene a la cabeza la siguiente pregunta: ¿Cuál es el siguiente paso?

Para poder ayudarnos entre todos y darle una respuesta a esta pregunta, desde este blog vamos a brindarte información sobre algunas propuestas interesantes, que salen de lo rutinario y, si se realizan con toda voluntad, pueden ser muy educativas además de entretenidas tanto para tus beneficiarios o tus compañeros de patrulla, equipo o comunidad. ¡Toma nota!




## Campamento Virtual



Muchos educadores y beneficiarios saben o comprenden el trabajo que conlleva realizar un campamento, desde la concepción de la idea hasta la realización de las actividades que se tenían pensadas, es un trabajo incesante para que todo salga perfecto. Pero… ¿Cómo realizo un campamento virtual? Bueno, vamos a tratar de darte algunos puntos para que tengas en cuenta y, si quieres, puedas aplicarlos.

Primero y, antes que nada, tiene que haber una buena investigación previa sobre cómo están los estados de ánimos de mis compañeros o beneficiarios, ya que esto es fundamental para que todo nuestro trabajo se aproveche y se disfrute al máximo. Alguien cansado de estar frente a la computadora por temas de estudios o trabajo, capaz no le sienta bien seguir esta idea.


{{< img src="https://i.postimg.cc/Y21FKBK2/image.png" >}}


Una vez que sepamos que este proyecto es viable, vamos a tener que dar un ambiente para que se sienta un campamento, que se trata de una situación diferente a la rutina, para ello se puede pensar en el armado de sectores de acampe o carpas que le puedan dar un toque mágico a este momento.

Establecer horarios óptimos de trabajo a la hora de las actividades ayuda a la concentración sobre las mismas y no sobrecargan la mente, como así también, considerar la frecuencia en la que establecemos las reuniones, ya que no queremos generar un desgaste, tanto en el animo como de nuestra vista, al estar tanto tiempo frente a los aparatos electrónicos. Es bueno también poder variar de plataformas para que se pueda originar una expectativa diferente. También se puede generar espacios de intercambio en el horario de las comidas entre integrantes de patrulla, equipos o comunidad, e incluso compartir algún taller de cocina que se pueda llevar a cabo en el momento. Y, nunca puede faltar en cualquier campamento, alguna velada o fogón simbólico al horario de la noche, que nos proporciona un momento mágico.     




## Sábados desconectados



¿Dejar de hacer actividades algún sábado? No, no nos referimos a eso cuando hablamos de “sábados desconectados”, se apunta a un momento de reflexión. En la vida de muchos caminantes y/o rovers, el raid o el campamento en soledad respectivamente, significo un momento muy especial en el cual se han encontrado con ellos mismos y los ayudo a crecer. Entonces… ¿De qué se trata esta propuesta? De que podamos realizar actividades un sábado, pero sin la necesidad de tener que conectarse a una videollamada, que sea un momento donde podamos realizar una pequeña meditación para que nos podamos encontrar con nosotros mismos, descubrir cosas nuevas y plantearnos nuevos caminos. Para ello, nos podemos apoyar en el mundo vasto que es el internet y la cantidad de material que encontramos sobre este tema, ya sea alguna lectura o algún contenido audiovisual, que a veces es más interesante para las generaciones actuales. También pueden estar presente las preguntas orientativas, que son siempre bienvenidas y nos apuntan hacia donde queremos llegar.  


{{< img src="https://i.postimg.cc/nzxHq5dT/image.png" >}}


Esta propuesta se puede realizar como punto de partida para la iniciación o revisión de la progresión personal de los chicos. Lo único a considerar a la hora de llevar a cabo, es el compromiso personal que puedan tener nuestros beneficiarios para desenvolverse solos en estos temas.     




## Olimpiadas scout



Nada mejor como una contienda entre equipos para reavivar la llama de una sana competencia en base a los valores y principios de nuestro movimiento. Los Juegos Olímpicos (JJ.OO) (o también Olimpíadas) son el mayor evento deportivo internacional del mundo, en el que compiten atletas representantes de prácticamente todos los países existentes (alrededor de unos 200 en total), a lo largo de varios días de un evento multidisciplinario organizado cada cuatro años. Muchos habremos hecho alguna olimpiada scout en el patio de grupo en base a juegos característicos del movimiento, ya sea para una competencia interna en el grupo, un evento distrital o cualquier otra situación. Pero, a la hora de realizar esta actividad en la modalidad virtual, las disciplinas físicas pueden ser relativas ya que poseen cierta dificultad para concretarlas (Se puede considerar el tema de ciertos elementos básicos del ejercicio como, por ejemplo, quien hace mas flexiones de brazos, abdominales, etc.) y, entran en juego otras aptitudes como lo son: la creatividad, el ingenio, el conocimiento, entre otros.


{{< img src="https://i.postimg.cc/9XJWqLyt/image.png" >}}


Para esta propuesta te podemos recomendar algunas cosas que harán esta experiencia memorable: Realizar alguna apertura o ceremonia con música característica, establecer un cronograma al que todos tengan acceso para poder prepararse tanto física como mentalmente, realizar competencias tanto individuales como por patrullas o equipos para fortalecer lazos y, por supuesto, una entrega de medallas simbólica para reconocer aptitudes excepcionales.      




## Desarrollar intereses personales



Muchos de nuestros beneficiarios o compañeros se quejan de aburrirse en el colegio, de que las materias que les enseñan no le son relevantes y de las tareas que les asignan a veces pueden ser demasiadas. Además, sienten que nada de ello se relaciona con sus intereses personales.


{{< img src="https://i.postimg.cc/pdx7CHH9/image.png" >}}


Si bien como educadores nuestro deber es formar agentes de cambio para la sociedad, y en este deber es fundamental la educación, también tenemos que destacar como un objetivo central la necesidad de capacitar a los niños, niñas y jóvenes en reconocer y desarrollar sus motivaciones. Estas aspiraciones creativas también suponen que los educadores favorezcan que los beneficiarios se hagan conscientes de su potencial, para llevar una vida plena y productiva que permita la exploración de distintos ámbitos, el error que implica las elecciones personales y la aceptación de las mismas. Nunca está de más la realización de talleres para aprender cosas nuevas, y más sabiendo de la cantidad de tutoriales que existen en internet para poder hacer cualquier cosa que nos imaginemos.



---



Como ya dijimos, son recomendaciones que podemos dar desde nuestro lugar, usa las que creas que son buenas, recomendémonos entre todos actividades e ideas que pueden dejar escritas en nuestros comentarios para generar un gran contenido de actividades scout entre todos, y siempre impongámosles nuestra impronta para que salga lo mejor. Saludos y ¡Siempre Listos!
