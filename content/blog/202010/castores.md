---
title: '¿Por qué los castores?'
date: '2020-10-09 13:28:00'
description: '¿Hay algún limite en el movimiento scout? ¿Cuál es el rango de edad donde se le puede enseñar a alguien los valores que inculca el escultismo?'
type: 'publicaciones'
categories: ["raiders","tropa","rovers","manada"]
color: "universal"
theme: 'opiniones'
tags: ["lectura", "manada", "tropa", "unidad", "caminantes", "raiders", "opiniones", "principito", "castores", "ramas"]
img: 'https://www.scouts.org.ar/wp-content/uploads/2017/04/castores.png'
authors: ["Norberto Daniel Ceballos"]
---



¿Hay algún limite en el movimiento scout? ¿Cuál es el rango de edad donde se le puede enseñar a alguien los valores que inculca el escultismo? Muchas veces hemos vistos adultos, que nunca antes han participado de las actividades, se suman para ser Educadores y mientras transitan ese camino van aprendiendo muchas cosas, por ende, personas que pueden tener 30, 40 o 50 años se sienten atrapados por todo lo que significa ser scout, pero… ¿y si se quiere que se aprenda desde más jóvenes que un lobato?


{{< img src="https://www.scout.es/wp-content/uploads/castores_secciones.jpg ">}}


En el día de hoy, a través de esta nota en este blog, vamos a conocer un poco más esta nueva rama que se creó en algunos países que practican el movimiento scout y abarca a los nenes de 5 a 7 años. Pero primero, pongamos en contexto: Cuando Baden Powell presentó a la sociedad al Movimiento Scout, lo hizo observando lo que pasaba con los muchachos de entre 11 y 14 años y queriendo proponerles una forma de educarse en su tiempo libre que fuera útil para ellos y para la sociedad en la que vivían. A todo esto, se sumó el impulso de Vera Barclay, que describía:



> Estaba de actividades con mi Tropa Scout y un montón de muchachitos venían y nos interrumpían: ´Señorita, si los Scouts nos quieren estamos listos´. Cuando vi a estos niños pequeños y espabilados saludar con los tres dedos pensé que algo debía hacerse por ellos”



Así, en el año 1916 nació, después de un gran proceso de modificaciones y trabajo, la rama “Manada”. Conociendo este contexto, desde un lado personal, muchas veces he visto a nenes chiquitos, que por un tema de edad no entraban a la manada, pero que compartían con una sonrisa las actividades y se los veía con muchas ganas de aprender y participar, entonces… ¿Por qué no darles un lugar para que aprendan los valores que a muchos nos inculcaron y amamos?


{{< img src="https://docplayer.es/docs-images/84/90760304/images/51-0.jpg" >}}


Primero, para comprender esta nueva rama, introduzcámonos un poco en el tema de como es un nene de esa edad: Son chicos llenos de energía, comienzan a desarrollarse desde lo físico y lo motriz, amplían enormemente su vocabulario, pueden expresar sus estados de ánimos y sus ideas, aunque de una manera desordenada, participan en los juegos juntos a otros chicos y disfrutan de su compañía y lo más importante, quieren aprender cosas nuevas a través de los juegos, ¿De dónde nos suena esto?




## ¿Cómo funcionan los castores?



Primero y, antes que nada, quiero aclarar que lo que se va a decir es en base a los conocimientos que pueda tener sobre esta rama a través de la lectura y lo que se ve en el patio de grupo. Pero, no conforme con esto, hablamos un poco con un dirigente castor, que nos conto su experiencia en primera persona:




#### ¿Cómo es trabajar con los castores, desde la vista de un educador?



> “Es una tarea que implica la misma responsabilidad y preparación que el resto de las ramas, pero si se debe poseer un perfil característico: se necesita una personalidad muy positiva y una cintura para manejar la intensidad de los mas chiquitos que llegan al grupo. Son un grupo de niños y niñas muy especial por la edad, que requieren un acompañamiento adecuado, por eso el Adulto tiene que potenciarse en habilidades y características singulares en la personalidad para estar a la altura de las circunstancias. Pero no deja de ser muy gratificante, ya que su forma de ver la vida sin prejuicios y ser tan espontáneos, que se ven manifestados en sus palabras, acciones y su afecto”



El rol del educador es sumamente importante. Entre las cualidades generales se encuentran las de poder acompañar a los niños en su desarrollo, brindar confianza y seguridad, ser divertido, cuidadoso, creativo y organizado, sin olvidar generar un ambiente donde los niños/as se sientan reconocidos, queridos y respetados. Conociendo un poco más sobre como es el adulto, nos preguntamos:




#### ¿Cómo son las reuniones sábado a sábado en un patio de grupo?



> “Las reuniones son planificadas, con una estructura y momentos marcados ya que a los mas chicos los ayuda a trabajar. Los principales fundamentos es que las actividades sean dinámicas y cortas, que comprendan su tiempo de atención. Estas actividades pueden ser juegos, talleres (una herramienta que se utiliza mucho ya que se pueden expresar libremente en sus diferentes áreas). Poseen un juego simbólico, característico para su edad, y todo esto se considera en un tiempo normal de reunión.”



La Vida de Grupo es el resultado de todo lo que pasa en la Colonia y de las relaciones que se establecen entre sus miembros. El atractivo de las actividades que se realizan, todo lo que ocurre en la Colonia va construyendo progresivamente una atmósfera especial, donde cada niño se desarrolla libremente. El juego simbólico que se nombra, no es un juego didáctico, es una expresión en forma lúdica de sus pensamientos, puede que aprendan algo o no aprendan nada, en todo caso su importancia radica en que estimula el pensamiento, sirve para expresar emociones y resolver situaciones que pueden presentarse como conflictivas, entre otros aspectos relacionados con el desarrollo evolutivo. Conociendo un poco más del sábado a sábado, nos centramos en otro elemento especial:




#### ¿Cuáles son los elementos del marco simbólico de la rama? ¿Qué es lo que hace especial a la rama?



> “Al igual que la manada, el ambiente de trabajo para los castores es la fantasía, y los elementos que se utilizan para este ambiente es el cuento ´Loney, Nehuen y los castores´, su banderín, el nombre que posea como comunidad que corresponde a un valor, las máximas y su color característico junto con su lema ´Vamos a compartir´”



El cuento relata la historia de dos nenes, que conocen a un grupo de castores trabajando por el bien común de la colonia. En el relato pueden observar como ellos trabajan, se ayudan, cuidan el medio que los rodea y transmiten la alegría de compartir todos juntos. Estas características de los castores como animales sociables, son un ejemplo a seguir que los niños pueden imitar para desarrollar nuevos valores junto a chicos de su misma edad.


{{< img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSuo9kJnRZSC3dFSCfXlFIRJATu-bysr5wIvQ&usqp=CAU" >}}


Otro de los elementos que nos pueden sonar raro son las máximas… ¿Qué son o que simbolizan? Las máximas son los pequeños acuerdos y los amigos que lo acompañan permiten que cada uno pueda construir compromisos por períodos cada vez más prolongados. Este es el inicio hacia la construcción de una adhesión voluntaria a la Ley y Promesa que realizarán en un momento posterior (Manada, Scout, Caminante o Rover).



En este pequeño articulo pudimos revisar algunos de los puntos fundamentales que se trabajan en esta nueva rama, pero al adentrarnos, es un mundo mas grande de lo que imaginamos con un montón de oportunidades para trabajar con los miembros más jóvenes que tiene el Escultismo. Sigamos con este camino que es el generar miembros activos en la sociedad con valores y siempre firmes para hacer el bien, como alguna vez lo dijo Baden Powell:



> Nuestro método de formación es educar desde dentro, en lugar de instruir desde fuera: ofrecer juegos y actividades que además resulten atractivos para el muchacho y que lo eduquen seriamente en el aspecto moral, mental y físico.”



¡Siempre Listos!
