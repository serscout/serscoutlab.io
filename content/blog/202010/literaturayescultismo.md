---
title: 'La literatura y el Escultismo'
date: '2020-10-09 09:46:00'
description: 'Si bien muchos mantienen la lectura por placer a través de los libros, se ha cambiado la forma de leer y de mirar.'
type: 'publicaciones'
categories: ["raiders","tropa","rovers","manada"]
color: "universal"
theme: 'opiniones'
tags: ["lectura", "manada", "tropa", "unidad", "caminantes", "raiders", "opiniones", "principito", "demian", "eduardo", "galeano"]
img: 'https://i.postimg.cc/vTz1NQ9Y/image.png'
authors: ["Norberto Daniel Ceballos"]
---


{{< img src="https://i.postimg.cc/vTz1NQ9Y/image.png" >}}


¿Cómo es leer en el siglo XXI con esta generación? Si bien muchos mantienen la lectura por placer a través de los libros, se ha cambiado la forma de leer y de mirar. Ahora, además de libros, leemos y miramos pantallas. Esto altera irremediablemente nuestra concepción y aprendizaje de conocimientos, porque la pantalla no es solo un cambio de soporte, sino una profunda modificación en el modo de organizar los contenidos y como captamos los mensajes que nos quieren transmitir.

En varios artículos de los que venimos escribiendo se puede observar como los elementos del escultismo; como la ley, sus principios o valores, están inversos en muchas de las cosas cotidianas que nos rodean, así que ahora es el turno de la Literatura.

Sabemos que Baden Powell fue un gran escritor que, dentro de su legado, nos dejó varios libros que están dedicado a ramas específicas del movimiento, como “Escultismo para muchachos” o “Roverismo hacia el éxito”, entre los mas conocidos. Pero, el enfoque de esta nota estará apuntando hacia libros que capaz sean conocidos para ustedes, o tal vez no, y que dejan un mensaje que esta muy relacionado con lo que podemos trabajar sábado a sábado. Estos libros pueden servir tanto para una lectura en una tarde de ocio, como para, si sos dirigente, usarlo como un marco referencial en algún campamento, así que… ¡Buena Lectura!




## **“El Principito”**

#### Antoine de Saint-Exupéry


{{< img src="https://i.postimg.cc/QMGTCnXr/image.png">}}


Capaz uno de los libros mas conocidos y que se han usado para muchas actividades scout, pero aun así vale la pena su mención. El principito es una narración corta, que trata de la historia de un pequeño príncipe que parte de su asteroide a una travesía por el universo, en la cual descubre la extraña forma en que los adultos ven la vida y comprende el valor del amor y la amistad. Es tenido como uno de los mejores libros de todos los tiempos y un clásico contemporáneo de la literatura universal. Debido a su estilo sencillo y directo se lo puede considerar un relato para la manada, aunque, su profundo carácter reflexivo sobre la vida, la sociedad y el amor, lo convierten en una narración de interés para todas las ramas. Dentro de la historia, encontramos un personaje que siempre dice la verdad y que constantemente se cuestiona, busca y persiste en encontrar la respuesta, por muy simple que parezca la pregunta, En sus conversaciones con el Principito, el narrador revela su visión sobre la vida humana y la profunda sabiduría que nace de la sencillez propia del corazón de un niño, algo que podemos perder con el paso del tiempo y el choque con las circunstancias. Ciertamente, no se trata de ver la vida con ingenuidad, que eso sólo lleva a la amargura y a la decepción, sino de no perder la esencia interior de uno mismo. Se trata de ver la vida desde lo que vale de verdad, no desde lo que nos permite sobrevivir, sino desde lo que nos hace vivir con mayúsculas. Esa es la gran lecciónde este hermoso libro.    




## **“Demian”**

#### Hermann Hesse



Un libro con un profundo valor reflexivo. Este escritor posee varios títulos que nos pueden generar una gran introspección. Obras como “Siddhartha“ o “El lobo estepario” son grandes libros de este autor, pero el caso que vamos a nombrar tiene su toque especial. En Demian resuenan hechos vibrantes de las reflexiones del autor sobre la adolescencia atormentada; de ese tiempo de búsquedas, dolores y sufrimientos. Es una obra que refleja el espíritu de la época y sus notables influjos culturales. Se muestra los valores o las inquietudes de los adolescentes al querer ser aceptados, llevándolos a elegir opciones que no les favorecen; pero llegando a la maduración del personaje. Emil Sinclair es un personaje que ha vivido siempre conforme a la moral correcta y que cree que ser bueno es su único cometido, no conoce todavía la maldad del mundo. En su camino, aparecerá Max Demian, un personaje muy importante que da título a la novela y que obligará a Emil a cuestionarse todas las cosas que ocurren en su vida de forma filosófica y crítica. Es, digamos, una especie de guía que le llevará a interrogarse constantemente, a descubrir la cara oculta del mundo y a romper con todos aquellos paradigmas que le fueron inculcados.


{{< img src="https://i.postimg.cc/0Njy8YSq/image.png" >}}


 A la hora de involucrarlo al escultismo, claramente se identifica con una etapa de los chicos en la cual están buscando su camino, por eso, se puede dirigir a beneficiarios tanto de la tropa, (Aunque ya deberían estar avanzado en su paso por la rama), los caminantes o Raiders, y Rovers que todavía no eligieron su camino.




## Eduardo Galeano



Uno de los autores más conocidos de Latinoamérica, nacido en Montevideo, Uruguay, el 3 de septiembre de 1940. Fue periodista y escritor de grandes títulos que poseen traducción a más de veinte idiomas. Es, sin duda, uno de los cronistas de trayectoria más incisiva e inteligente de su país. Una de sus obras más conocidas es Las venas abiertas de América Latina, un análisis de la secular explotación del continente sudamericano desde los tiempos de Colón hasta la época presente que, desde su publicación en 1971, ha tenido más de treinta ediciones. Pero a la hora de buscar algún libro para scout, ¿Cuál es la recomendación?

La respuesta a esa pregunta es… ninguno. Si bien todos los libros que escribió poseen un gran valor desde su desarrollo, encasillar un solo libro para leer seria poco. Si queres armar un campamento o alguna actividad, te recomiendo la utilización de sus relatos, podes ir seleccionando de varios libros y apuntar hacia donde gustes, es lo que te permite la versatilidad del escritor.


{{< img src="https://i.postimg.cc/7Z6k7xZ3/image.png" >}}


Estos son solo de algunos de los libros que presentamos para que puedas leer, (pueden existir muchísimas más), y compartir este maravilloso mundo de la lectura, que nos llena de valores que también podemos encontrar en el escultismo. Si vos conoces y te gustaría compartir otro, deja un comentario y entre todos vamos construyendo una gran literatura para los Scouts.     



