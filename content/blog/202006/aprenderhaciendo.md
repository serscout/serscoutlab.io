---
title: 'Aprender haciendo: La razón del escultismo'
date: '2020-06-22 14:37:00'
description: 'Somos diferentes, porque hacemos mientras aprendemos, y aprendemos mientras hacemos, eso nos diferencia de otras instituciones y queremos contarte más.'
type: 'publicaciones'
categories: ["raiders","tropa","rovers","manada"]
color: "universal"
theme: 'noticias'
tags: ["noticias", "aprender", "tropa", "unidad", "caminantes", "raiders", "haciendo"]
img: 'https://eduskopia.com/wp-content/uploads/2013/06/DSC_0049_2-470x290-e1372585375376.jpg'
authors: ["Norberto Daniel Ceballos"]
---


{{< img src="https://eduskopia.com/wp-content/uploads/2013/06/DSC_0049_2-470x290-e1372585375376.jpg" >}}


*¿Mesa con banco o el pasto? ¿Pizarrón o naturaleza? ¿Horarios de clase o jornada de actividades en el patio de grupo?* Muchos de los que estamos en el movimiento Scout nos sentimos atraídos por la forma en la que aprendíamos cosas nuevas: poniendo manos a la obra sobre situaciones que nos ocurrían; y ahí es donde se encuentra la magia del Escultismo.

En este articulo no vamos a centrarnos si la educación Formal (la que se aplica actualmente en los institutos educativos) es buena o mala, vamos a centrarnos en una de las cosas que hace atractivo a los Scouts: El **“Aprender haciendo”**. Pero… ¿Qué es esto de aprender haciendo?, Baden Powell escribió sobre esto y nos dejó una respuesta:



> “El principio motriz del escultismo es estudiar las ideas del muchacho y animarlo a que se eduque por si solo en vez de esperar a recibir una instrucción, (…), El jefe de tropa transmite al joven el ansia de aprender por sí sólo.”



Como primer punto, se puede decir que el aprender haciendo privilegia el hacer por sobre el hablar, y se marca la diferencia entre el protagonismo del joven en el Movimiento Scout y la actitud pasiva de escuchar una conferencia o mirar una demostración. Todo esto se realiza para obtener conocimientos, habilidades y actitudes sobre objetivos establecidos por ellos mismos. Pongamos un ejemplo claro: el integrante de la tropa “X” quiere aprender a hacer un fuego pirámide, entonces plantea esta necesidad a su patrulla o a sus dirigentes; y acá es donde se ve la importancia de este método: en vez de recibir un video tutorial o una clase, pone sus manos a la obra, con los materiales necesarios, el cómo realizar ese fuego especifico junto a sus compañeros de patrulla o gracias a la actividad de un dirigente y lograr ese objetivo que se había planteado


{{< img src="https://i.pinimg.com/originals/41/08/c0/4108c0ce91118fb1d60b72ed5fb7241c.jpg" >}}


Podríamos quedarnos en esa definición, pero el aprender haciendo va más allá del simple "hacer", es más que aprender las habilidades prácticas o manuales, se trata de esa aventura del descubrimiento que puede tomar la forma de un juego, de un campamento, de un servicio a la sociedad.  Por último, en esto del aprender haciendo siempre hay algo que debe ser fundamental: se basa en que alguien que ya conoce la técnica muestre y acompañe en los primeros pasos para la obtención de la habilidad, esto nos permitirá aprender a través de las oportunidades de experimentación que surgen al seguir los intereses de uno, pero acompañado a través de ese grupo especial que nos rodea.
Muchas de estos artículos tienen un toque personal, por eso, podemos llegar a pensar diferente en esto del “Aprender haciendo”, pero lo importante es que entre todos construyamos este blog y que en los comentarios nos dejen que piensan que es lo lindo o atractivo del Escultismo, los vamos a estar leyendo. Saludos y…

¡Siempre Listos!

*Fuente: “Guía para el jefe de Tropa”, Lord Baden Powell of Gilwell (1919)*
