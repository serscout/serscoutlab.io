---
title: 'Recetario Scout I'
date: '2020-06-15 17:49:00'
description: 'Iniciamos con esta seccion de Recetario Scout donde podras ver las mejores recetas para tus campamentos'
type: 'publicaciones'
categories: ["raiders","tropa","rovers"]
color: "universal"
theme: 'cocina'
tags: ["campamento", "supervivencia", "tropa", "unidad", "caminantes", "raiders", "cocina"]
img: 'https://i.postimg.cc/3NhkwYNH/image.png'
authors: ["Norberto Daniel Ceballos"]
---

### “Del fogón a tu plato enlozado”



¿Quién dice que la comida de campamento tiene que ser monótona? ¿o qué siempre tienen que estar presentes los mismos platos? En este blog, vamos a abrir una nueva sección donde puedas tomar estas recetas y **sorprender** a tus compañeros de patrulla o de comunidad.


{{< img src="https://i.postimg.cc/3NhkwYNH/image.png" >}}


Hoy, en *“Recetarios Scout”* veremos tres platos, capaz conocidos para algunos y desconocidos para otros, pero que se pueden preparar y siempre van a dar una sorpresa. Todas estas recetas tienen un punto en común: ¡No utilizaremos utensilios!, solo contaremos con un cuchillo y con un buen compañero a nuestro lado, el papel aluminio. Así que… *¡Manos a la obra!*




## Zapallo Relleno



Valido como un acompañamiento y como para un plato principal, es una receta llena de sabor que se puede disfrutar en equipo. Lo único a tener en cuenta, es el tiempo de preparación, ya que si la apuramos podemos terminar comiendo calabaza cruda.


{{< img src="https://i.postimg.cc/59q8hkKC/image.png" >}}


* **Tiempo:** 60 minutos (Aprox.)
* **Ingredientes:** Zapallo, queso, Sal, pimienta, aceite y ¡lo que quieras para rellenar!



**Preparación:** Para esta receta necesitamos la mitad de un zapallo, al cual lo vamos a tener que limpiar quitando todas las semillas, y cuando este limpio, se le agrega sal y pimienta a gusto, más un pequeño chorro de aceite. Una vez que hayamos hecho todo eso… ¡es el momento de rellenar! Para esto podés agregar lo que se te venga a la cabeza (choclo, arvejas, cebolla*, morrón*, jamón cocido, etc.), todo junto al queso cortado en cubos para que se derrita más rápido. Trata de llenar hasta la mitad del zapallo para que no se te rebalse después.

La mejor técnica de cocción es formar un círculo de brasas y poner el zapallo en el medio: no debe estar en contacto con el fuego directo porque podría romperse (Se puede usar una tapa de papel aluminio para lograr un buen punto). Para saber si esta listo, se puede hundir un cuchillo en el zapallo, si pasa sin dificultad, ya estará a punto para comer.

* A gusto personal, estos ingredientes se pueden agregar ya cocinados.




## Papa a la brasa



¡La papa! Un elemento tan fiel a cualquier comida y que es autóctona de nuestra hermosa América. Se puede cocinar de miles de formas y en todos los casos queda excelente, pero con esta receta vas a alucinar…


{{< img src="https://i.ytimg.com/vi/f4u_GrpFlEM/hqdefault.jpg" >}}


* **Tiempo:** 30minutos (Aprox.)
* **Ingredientes:** Papa, papel aluminio, cebolla, morrón, sal, pimienta y aceite.



**Preparación:** Una preparación bastante sencilla que puede tener un sinfín de sabores. Lo que tenemos que hacer es: a la papa, que la dejamos con la cascara y bien lavada, la cortamos en rodajas, pero sin llegar hasta el final, como se ven en la foto. Una vez que se logró eso se la coloca sobre papel aluminio y la acompaña con las verduras que nosotros queramos, en este caso coloque cebolla y morrón cortadas en rodajas o picado, como mas te guste. Por encima se coloca un chorro de aceite y salpimienta a gusto (hasta se puede agregar condimentos si tenemos a mano), se envuelve en el papel aluminio y se coloca a las brasas por un tiempo de 30 minutos.

Una vez pasado el tiempo, se retira del fuego y se deja enfriar para luego poder servirlo. Si queremos estar seguros sobre la cocción de la papa, se le puede atravesar un cuchillo, si sentimos que traspasa fácilmente, esta perfecta.  Cabe resaltar que esta receta se hizo con tres verduras solamente, pero podés agregarles todas las que te parezcan: ajos, chaucha, entre otros, ¡Solo es cuestión de experimentar y de nuestros gustos!    




## Morrón relleno



Esta receta simple y rápida tiene un gusto que nunca van a olvidar, sus compañeros de equipos van a quedar sorprendidos y, los aplausos van a llover para el cocinero.


{{< img src="https://www.deliciosi.com/images/2200/2287/morr%C3%B3n-a-la-parrilla.jpg" >}}


* **Tiempo:** 30 minutos (Aprox.)
* **Ingredientes:** Morrón, papel aluminio, Jamón cocido, queso, huevo y condimentos.



**Preparación:** Para arrancar con esta receta, se debe tener un morrón grande, al cual debemos cortarlo a lo largo, quitar las semillas y las partes blancas (Importante: No sacar la totalidad del tallo, dejar la parte verde, para que se pueda retener el huevo). Luego se lleva a cocinar a las brasas boca abajo, en vuelto en papel aluminio, por unos 10 minutos para que se ablande un poco.

Una vez pasado el tiempo se retira del fuego y se rellena en el siguiente orden: colocamos el jamón y el queso juntos, se condimenta con lo que nos pueda gustar, y por encima de todo eso se coloca el huevo. Con cuidado lo volvemos a colocar, envuelto en el papel aluminio, a las brasas el tiempo necesario para que se cocine el huevo (tanto la yema como la clara).

No te hagas problema si en la base del morrón se nota que la piel está un poco quenada, ya que esa es solo una capa que, si quieres, podes retirar fácilmente.



---



¡Es momento de poner tus manos a la obra!, estas recetas pueden servir de guía, pero a todo lo que prepares ponele tu toque especial y hacelas propias. Si queres compartir alguna receta, déjala en los comentarios y entre todos construyamos un estilo culinario de los Scout. ¡Siempre Listo!
