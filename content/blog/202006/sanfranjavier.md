---
title: 'Patronos: San Francisco Javier'
date: '2020-06-05'
description: '¿Alguna vez nos preguntamos por qué para cada rama se eligió un patrono o patronos en particular? '
type: 'publicaciones'
categories: ["raiders"]
color: "raider"
theme: 'historias'
tags: ["historias", "javier", "francisco", "patronos", "caminantes", "raiders"]
img: 'http://2.bp.blogspot.com/_GLljLX0plI0/STMZmHhcpDI/AAAAAAAABAQ/zF9R873aUx8/s320/SanFranciscoJavier.jpg'
authors: ["Norberto Daniel Ceballos"]
---

### “Si no consigo barco, iré nadando”



¿Alguna vez nos preguntamos por qué para cada rama se eligió un patrono o patronos en particular? ¿Cómo fueron sus vidas para que hoy en día muchos Scouts los recuerden en sus oraciones? Bueno, hoy nos encaminaremos en la vida de uno de ellos, que se puso a disposición para ayudar a los demás y predicar la palabra de Dios.

Cuando se transita la rama caminantes o raiders, (como la quieran llamar), se viven un montón de aventuras durante las caminatas o rallys que se pueda llegar a hacer, de eso se trata esta etapa, ¿no?, caminar junto a mis compañeros de equipo o comunidad y tener un montón de anécdotas de esos momentos descubriendo una aventura a cada paso.


{{< img src="http://2.bp.blogspot.com/_GLljLX0plI0/STMZmHhcpDI/AAAAAAAABAQ/zF9R873aUx8/s320/SanFranciscoJavier.jpg" >}}


La vida de San Francisco Javier, patrono de los caminantes, fue una aventura constante y por cada paso que daba hacia alguna dirección, llevaba consigo una cosa fundamental: la palabra de Dios. Nació en 1506, en el castillo de Javier en Navarra, España; y desde joven se orientó hacia la carrera eclesiástica y el cultivo de las humanidades. A los dieciocho años fue a estudiar a la Universidad de París, en el colegio de Santa Bárbara, donde en 1528 obtuvo el grado de licenciado. Pero ese viaje no solo le daba un título de su carrera, sino que también le cambiara drásticamente la vida, ya que comienza su relación de amistad con Ignacio de Loyola, quien pretendía atraerle para el proyecto de fundación de una nueva orden religiosa.  Al principio Francisco rehusó la influencia de Ignacio, pero poco a poco fue calando y retando en sus pensamientos. Por fin San Ignacio logró que Francisco se apartara un tiempo para hacer un retiro especial que el mismo Ignacio había desarrollado. Fueron estas cosas que determinaron a Francisco a abandonar sus pretensiones de promoción dentro del estamento eclesiástico. Junto con Ignacio de Loyola, (y otros cinco compañeros), hicieron votos de castidad y pobreza, de peregrinar a tierra santa predicando la palabra y poniéndose a disposición del Papa.

En 1537 se trasladó a Venecia, donde se reunió con sus compañeros con el objetivo de viajar a Roma para obtener la bendición papal antes de iniciar su peregrinación a Tierra Santa, pero recibió la noticia del inicio de la guerra entre Constantinopla y Venecia, lo que significaba el retraso indefinido de su viaje. Ante la tardanza del mismo, viajo a Roma y se ofreció al Papa para ser enviados a cualquier otro lado.


{{< img src="http://jesuitasaru.org/wp-content/uploads/2016/12/JAVIER.jpg" >}}


Durante su estancia en la Santa Sede gestionaron la fundación de una nueva orden religiosa, la Compañía de Jesús, a la que el Papa concedió su aprobación verbal en septiembre de 1539, y de allí parte hacia Lisboa en 1540, donde dará comienzo la etapa más importante de su vida: la de misionero.

Las misiones que hizo San Francisco Javier fueron muchas: Inicio el viaje en abril de 1541, donde arribó a Goa, capital de las posesiones portuguesas en la India y ejerció en esta ciudad una activa labor evangelizadora. De ahí en más, cabe aclarar que son impresionantes las distancias que Francisco Javier recorrió en la India, Indonesia, Japón y otras naciones; a pie y otras veces en barco, enseñando, atendiendo enfermos, bautizando gentes por centenares, aprendiendo idiomas extraños y por las noches colocándose junto al altar y encomendaba a Dios la salvación de esas almas que le había encomendado. El 3 de diciembre del año 1552 muere Francisco Javier cuando contaba 46 años de edad. Su cuerpo es conducido a Goa (una de las ciudades donde había comenzado su misión evangelizadora), donde llega en la primavera de 1554, siendo enterrado en esa ciudad.


{{< img src="https://www.hispanidadcatolica.com/wp-content/uploads/2019/12/francisco-javier-k24E-U601771312336C4C-624x385@Las-Provincias.jpg" >}}


Uno puede profesar la fe que sienta desde su corazón, o puede que todavía este en búsqueda, o simplemente sentir que la fe no es necesaria en su vida; pero en lo que todo podemos coincidir es que San Francisco Javier fue una persona que recorrió un montón de caminos y enfrento muchísimas dificultades; todo por cumplir su ideal, por ser fiel a la idea en la que él creía, sin importar el obstáculo que tuviera delante. Por eso, es el patrono de los Caminantes o Raiders, porque es un modelo a seguir para nuestros jóvenes en este mundo que puede poner trabas en las ideas que tienen o en los proyectos que quieran realizar. Por eso tengamos en cuenta esta frase que alguna vez dijo: *“Si no consigo barco, iré nadando”* y persigamos nuestros sueños hasta cumplirlos,



**¡Siempre Listo!** - **¡Salvar!**
