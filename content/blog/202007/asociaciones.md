---
title: 'Escultismo y asociaciones'
date: '2020-07-02 10:58:00.00'
description: 'A lo largo y ancho del mundo existen muchas asociaciones scouts....'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "opiniones"
tags: ["opiniones", "escultismo", "asociaciones", "escuela"]
img: 'https://losscouts40395247.files.wordpress.com/2018/06/floreslis-e1528982807311.jpg'
authors: ["Joaquin Decima"]
---


{{< img src="https://losscouts40395247.files.wordpress.com/2018/06/floreslis-e1528982807311.jpg">}}


A lo largo y ancho del mundo existen muchas asociaciones scouts. Cada uno puede tener sus preferencias pero en mi humilde experiencia vi como las asociaciones “modifican” el escultismo para su preferencia.
Guste o no esto es un gran problema porque así se va perdiendo mucho el escultismo y vi muchos grupos que pasaron de ser un grupo scout a un club de barrio… Los jóvenes tienen que vivir el escultismo real, obviamente en cada grupo existe una realidad y en base a esa realidad funciona el grupo.


{{< img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR1kdREALhjoRq9T0ZYO1WGAzyqn12ii57a8g&usqp=CAU">}}



## Mi experiencia



Yo tuve la suerte pasar por muchos grupos y con eso por varias asociaciones, como miembro o como invitado. Realmente entendi que más allá de lo que las asociaciones dicen, muchos grupos prefieren omitir algunas “reglas” con el fin de llevar el escultismo puro y ahí es donde vemos el verdadero amor por el escultismo.


{{< img src="https://4.bp.blogspot.com/-egQtbMJjYmg/VUIhlPpUOrI/AAAAAAAADJw/clhryCgEPEA/s1600/cruce-de-caminos.jpg">}}


En cambio otros grupos prefieren limitarse y mostrar un escultismo de aula algo tan alejado de la realidad que es impensable para todos aquellos que aprendimos haciendo…




## Ni vieja escuela ni nueva…



No existe una “escuela” que sea la que tiene la razón, cada contexto y cada grupo es un mundo. Sin embargo es una decisión de cada grupo / dirigente si decide hacer escultismo de salon o confrontar a los jóvenes con la necesidad de hacer y aprender...

{{< img src="https://i.postimg.cc/pdgDSRnL/image.png">}}

En la actualidad estoy en un grupo que pertenece a una asociación con la cual no estoy de acuerdo con nada (desde mi punto de vista volvió al escultismo una empresa) sin embargo en este grupo no se siente eso y eso se debe al staff que lo conforma que es capaz de separar la burocracia de una asociación con el escultismo de los jóvenes.
