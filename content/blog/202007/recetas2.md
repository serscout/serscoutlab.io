---
title: 'Recetario Scout II'
date: '2020-07-06 16:09:00'
description: 'Iniciamos con esta seccion de Recetario Scout donde podras ver las mejores recetas para tus campamentos'
type: 'publicaciones'
categories: ["raiders","tropa","rovers"]
color: "universal"
theme: 'cocina'
tags: ["campamento", "supervivencia", "tropa", "unidad", "caminantes", "raiders", "cocina"]
img: 'https://i.postimg.cc/zX0FnGzQ/image.png'
authors: ["Norberto Daniel Ceballos"]
---

### “Lo dulce de la cocina”



¿Quién dice que la comida de campamento tiene que ser monótona? ¿o qué siempre tienen que estar presentes los mismos platos? **En este blog**, vamos a abrir una nueva sección donde puedas tomar estas recetas y sorprender a tus compañeros de patrulla o de comunidad.


{{< img src="https://i.postimg.cc/zX0FnGzQ/image.png" >}}


Hoy, en *“Recetarios Scout”* veremos tres platos, capaz conocidos para algunos y desconocidos para otros, pero que se pueden preparar y siempre van a dar una sorpresa. Todas estas recetas tienen un punto en común: ¡Son un plato dulce!, que podemos disfrutar en un desayuno o una merienda de un campamento junto a mis compañeros. Así que… *¡Manos a la obra!*




## Bizcochuelo a la naranja



Cuando hacemos una planificación del menú para un campamento, en la merienda o en un desayuno suena muchas veces: Pan de cazador o galletitas… Probemos algo diferente, ¡una receta elaborada que es un manjar!


{{< img src="https://i.postimg.cc/k43QnVs2/image.png" >}}


* **Tiempo:** 40 min (Aprox.)
* **Ingredientes:** Dos naranjas, un huevo, Leche (1/2 taza), Masa para bizcochuelo (350g), papel aluminio, escarbadientes.



**Preparación:** Arrancamos con un cuchillo y una cuchara, el cual utilizamos para cortar una tapa en un extremo de la naranja (Como se ve en la foto) y vaciar toda la pulpa de la naranja, reservándola en un bol para utilizar más tarde. Es importante no romper la cascara mientras estamos sacando la pulpa, ya que va a ser nuestro molde en los pasos siguientes.

Cuando la naranja está vacía, trabajamos en el relleno: En el bol colocamos la base de bizcochuelo, la leche y los huevos; los batimos hasta lograr una masa media liquida. Se puede colocar la pulpa de la naranja que anteriormente separamos a esta masa liquida para terminar de darle un sabor especial.

Una vez todo listo, los vertemos dentro de la naranja hasta ocupar entre ½ y ¾ de la misma, la tapamos con la parte de la naranja que habíamos cortado al principio y le colocamos escarbadientes a modo de seguro para que no pueda salirse el bizcochuelo, lo cubrimos todo con papel aluminio y la posamos sobre las brasas por un tiempo aproximado de 20 minutos. Luego la retiramos del fuego, la dejamos enfriar, pelamos la cascara y… ¡Listo para disfrutar un bizcochuelo saborizado!




## Buñuelos



¡Una receta que es una fiel acompañante a cualquier scout!: Ingredientes fáciles de conseguir, se prepara rapidísimo y queda riquísimo acompañado con alguna infusión caliente en un campamento de invierno.


{{< img src="https://i.postimg.cc/qMPMD8v6/image.png" >}}


* **Tiempo:** 25 minutos (Aprox.).
* **Ingredientes:** 1 huevo, 1½ tazas de harina, ½ taza de azúcar, 1 Taza de leche, 1 pizca de sal, Aceite para freír.



**Preparación:** Para arrancar con esta receta, colocamos todos los elementos secos en un bol (Harina, azúcar y la sal) y los vamos incorporando de a poco, revisando que quede uniforme. Una vez que todos estos elementos están mezclados, se le agrega la leche con el huevo, y con un tenedor tratamos de obtener una masa homogénea, que no tenga grumos y se encuentre un poco espesa. En este paso, si se tiene, se le puede agregar un poco de esencia de vainilla o algún otro componente que le pueda dar un sabor diferente (Limón o naranja que son elementos que son mas probables que tengamos en un campamento).

Ahora que ya que la masa de los buñuelos esta lista, se calienta una olla o sartén con suficiente aceite. Cuando esté bien caliente, ve añadiendo porciones de masa con ayuda de un cucharón y vamos friendo los buñuelos hasta que se doren por un lado y por otro, durante unos 2-3 minutos máximo.

Una vez dorados, los retiramos y  lo vamos reservando en un plato con papel para eliminar el exceso de aceite y, como un detalle espectacular, los cubrimos con un poco de azúcar para decorar.




## Panqueques crocantes



No hay algo más rico que unos buenos panqueques con dulce de leche, pero a la receta tradicional de unos buenos panqueques, le damos una pequeña vuelta para dejarlos crocantes y con un sabor tremendo.


{{< img src="https://i.postimg.cc/T1QNLjN7/image.png" >}}


* **Tiempo:** 30 minutos (Aprox.)
* **Ingredientes:** 2 huevos, leche (Una taza), Harina (100g), aceite, manteca, azúcar, dulce de leche.



**Preparación:** Hacer panqueques es súper fácil. Primero se coloca la harina en un bol y añadí los huevos, el aceite y la leche, se mezcla bien hasta que se integren estos ingredientes y se vigila que no queden grumos. Una vez lista la masa, se calienta una sartén a fuego medio-bajo (Controlemos bien nuestro fogón) con manteca. Cuando la sartén esté caliente, añade un cucharon de la mezcla y la dejamos al fuego hasta que empiecen a salir burbujas en la superficie, entonces le damos la vuelta y cocina por el otro lado.

¡Pero esto no termina acá!, una vez listo todos los panqueques, le colocamos dulce de leche (O algún dulce con él lo queramos rellenar) y los cerramos en forma de cuadrado, para que no se salga el dulce. Se le coloca azúcar por encima y, en la misma sartén que tenemos caliente y con manteca, colocamos estos panqueques para que se doren y logremos ese crocante que le da un toque especial.



---



¡Es momento de poner tus manos a la obra!, estas recetas pueden servir de guía, pero a todo lo que prepares ponele tu toque especial y hacelas propias. Si queres compartir alguna receta, déjala en los comentarios y entre todos construyamos un estilo culinario de los Scout. ¡Siempre Listo!
