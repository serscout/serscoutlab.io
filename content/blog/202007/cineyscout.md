---
title: 'El cine y los Scout'
date: '2020-07-18 13:54:00.00'
description: 'El escultismo y el cine se pueden juntar y hoy vemos un poco como hacerlo'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "opiniones"
tags: ["opiniones", "cine", "scout"]
img: 'https://i.postimg.cc/8PBpHbfH/image.png'
authors: ["Norberto Daniel Ceballos"]
---


{{< img src="https://i.postimg.cc/8PBpHbfH/image.png">}}


Cuando escuchamos la frase _“Que la fuerza te acompañe”_ cada vez que la nombran en la saga de Star Wars, o _“Le voy a hacer una oferta que no podrá rechazar”_ de la voz característica de Vito Corleone en El Padrino, entre tantas otras frases características del cine, nos viene a la cabeza el recuerdo de cuando las vimos por primera vez y como nos sentimos en ese momento.

Cada película es un mundo nuevo en el cual nos sumergimos, pero… **¿Por qué nos gusta tanto mirar películas?** Toda necesidad humana de conectarse con cualquier forma de arte para inspirarse, es una gran experiencia. Una película es una mezcla de muchas formas de arte como imágenes, sonido, interpretación, etc., por lo que, en un momento dado, tu mente sin saberlo capta muchas formas de arte. Las películas te conectan directamente con los personajes que estás viendo en la pantalla y poco a poco te llevarán a su mundo, a lugares en los que nunca hemos estado y dentro de la piel de personas muy diferentes de nosotros mismos. Otra de las cosas que se nos puede ocurrir son los innumerables sentimientos que nos producen: ellas pueden hacernos reír, llorar, sufrir e incluso nos ayudan a encontrar la fuerza que necesitamos para enfrentar tus problemas.


{{< img src="https://hipertextual.com/files/2019/12/hipertextual-2010-decada-que-cine-dio-todo-perdio-todo-2019328882.jpg" >}}


Pero… **¿Qué relación podemos encontrar entre el Cine y los Scout?** Bueno, las películas nos pueden ayudar como herramienta para que podamos explicar cuáles son los conceptos de la rama en la que estamos, o para que podamos hablar sobre un tema que teníamos pensado, así que, bien utilizada, esta herramienta puede ser muy útil en cualquier momento.   

En este articulo vamos a recomendarte cuatro películas (No incluimos cosas como _“Zombie Camp”_ o cualquiera de ese estilo) que, desde lo que creemos, pueden representar bien los conceptos de cada una de las ramas del movimiento scout. Capaz no las conozcas o, quizás, ya la hayas visto, pero nunca está de más verlas otra vez, y usarlas para compartir con tus compañeros de equipo o de patrulla, trabajarlas en una actividad o como un marco para un campamento. Así que: _3… 2… 1… **¡ACCION!**_




## Tierra de osos (2003)


{{< img src="https://i.pinimg.com/originals/f8/ae/5b/f8ae5b28b38cb8cbc9fb039686875aee.jpg" >}}


Una de las películas de la época experimental de Disney que fue reconocida, pero, infravalorada. “Tierra de osos” trata sobre la hermandad y el amor, saber respetar al prójimo y desterrar el odio y la venganza. En los bosques del noroeste americano vive un niño llamado Kenai, cuya vida sufre un giro inesperado cuando los Grandes Espíritus lo transforman en un oso, el animal que más odia. Kenai se hace amigo de un osezno llamado Koda y se propone recuperar su forma humana. Mientras, su hermano (que no sabe que Kenai es ahora un oso) lo persigue para cumplir una misión de venganza en la que está en juego el honor familiar.


{{< img src="https://eltoper.com/wp-content/uploads/2019/10/confirmado-tierra-de-osos-volvera-en-su-version-live-action2.jpeg" >}}


Cuando pensamos en la manada, se nos viene a la cabeza un ambiente de fantasía, generalmente en lo que llamamos “La selva” (Basándose en “El libro de las tierras vírgenes” de Rudyard Kipling), un lugar donde transcurre la vida de nuestro lobato donde, además de recrear la fantasía, los dirigentes puedan establecer una comunicación con el mundo del niño, ser aceptado y proponer modelos a seguir. Todos estos fundamentales para la manada, se ven en la película: Cuenta como van ocurriendo transformaciones interiores que van convirtiendo al protagonista demostrándolo a través de ese camino que realiza, y mostrando como en ese sendero, siempre contamos con la ayuda de aquellos que ya lo han transitado antes que nosotros, aportándonos las claves para que podamos crecer. Muy recomendable además por los mensajes sobre cuidar la vida y la naturaleza.




## Star Wars (1977)


{{< img src="https://e00-marca.uecdn.es/assets/multimedia/imagenes/2019/12/18/15766807360292.jpg" >}}


¿Qué simboliza ser parte de la Unidad o Tropa? Es el comienzo de muchas aventuras junto a un grupo nuevo de amigos. Es la independencia de “Akela” y funcionar junto a mis compañeros en la patrulla. Pero, también estos cambios pueden representar incertidumbre en el chico, por eso proponemos la saga: “Star Wars”. Para esto nos basándonos en los personajes principales de las películas, Los caballeros jedis. Ellos son guardianes de la paz y guerreros con vocación espiritual de servicio a la comunidad.


{{< img src="https://i.blogs.es/69fdcc/star-wars-saga/1366_2000.jpg" >}}


En estas películas se enfatiza mucho en la ética, que es otra de las temáticas centrales de esta trepidante historia. No hay que olvidar que el eje central es la eterna lucha entre el bien (la Fuerza) y el mal (el Lado Oscuro de la Fuerza). La Fuerza representa la capacidad personal del individuo en muchos aspectos: la superación personal, la autoestima, el compañerismo. El Lado Oscuro representa el “ello” froidiano: Representa el odio, la rabia, la venganza, el egoísmo; proporcionando un binomio muy acertado a la hora de plantear nuestras actitudes en cualquier tipo de convivencia.



**NOTA: Las películas de Star Wars para las nuevas generaciones pueden ser tediosas, pero, lo que tiene esta saga, es que ofrece series o películas animadas que pueden ser mas accesibles.**




## En busca del destino (1997)


{{< img src="https://i.pinimg.com/originals/55/93/6e/55936ea4e4c93dc18d322c77516694d2.jpg" >}}


Cuando pensamos en los **Caminantes** o en los **Raiders**, pensamos en jóvenes que están pensado en que hacer de su vida, pero a la vez pueda estar mezclado con las ganas de vivir de fiesta o la de hacer cualquier otra cosa que implique vivir el momento. Por eso podemos recomendar esta película, ya que tiene todo lo que cualquier persona con disyuntivas emocionales podría necesitar. Muchos chicos de 14 a 17 años llegan a un momento donde no saben qué camino tomar o qué futuro preparar para sí mismos, y en este film lo representan de una manera particular: El de atreverse por esa remota oportunidad, o simplemente ir por el camino fácil y desechar la buena oportunidad que se tiene. Esta película cuenta la historia de Will Hunting, un joven prodigio y a la vez problemático que trabaja como personal de limpieza en la universidad de MIT; sin aprovechar en gran potencial que tiene con las matemáticas del nivel más alto, desperdicia el tiempo con sus amigos en bares y causando problemas. El profesor Gerald Lambó descubre su gran potencial y le da facilidades para escapar de ese mundo de mediocridad y vagancia. Will se enamora de Skylar, una chica de la Universidad de Harvard, y pasa por terapia con el psicólogo Sean Mcguire, quien le guiará para superar sus problemas y empezar a poner su inmensurable potencial intelectual a trabajar.




## Hasta el último Hombre (2016)


{{< img src="https://i1.wp.com/cinefilosfrustrados.com/wp-content/uploads/2017/01/244714_logo_poster-final-de-hasta-el-ultimo-hombre-el-regreso-de-mel-gibson-original_claim.jpg.jpeg" >}}


En el libro “Roverismo hacia el Éxito”, Baden Powell explica como un Rover va a vivir esta etapa: “encontrar uno su camino por senderos con un objetivo definido y teniendo una idea de las dificultades y peligros que van a encontrar en él”. Hasta el último hombre es una película que contiene todos estos conceptos y los profundiza de una manera espectacular, la personalidad del héroe es puesta a prueba de la peor manera, pero él nunca renuncia a ese ideal que se estableció y lucha por lograr su objetivo.


{{< img src="https://i.blogs.es/5c070b/andrew-garfield/1366_2000.jpg" >}}


Desmond Doss, un hombre que se opone a la violencia, se alista en el ejército de EEUU para servir como médico de guerra en plena II Guerra Mundial. Tras luchar contra todo el establecimiento militar y enfrentarse a un juicio de guerra por su negativa a portar un arma, consigue su objetivo y es enviado a servir como médico al frente de guerra en Japón. A pesar de ser recibido con recelo por todo el batallón durante la salvaje toma de Okinawa, Desmond demuestra su valor a través de sus acciones.



**Nota: La película se basa en una historia de la vida real.**




---



Estas son solo de algunas de las películas que presentamos para cada una de las ramas, (pueden existir muchísimas más), que se pueden utilizar para que puedan realizar actividades variadas y entretenidas con nuestros compañeros/beneficiarios de rama, o solamente para poder disfrutarlas. Si vos conoces y te gustaría compartirla, deja un comentario y entre todos vamos construyendo una gran filmografía para los Scouts.


