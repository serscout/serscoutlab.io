---
title: 'No Hubo Dragones'
date: '2020-07-24 15:38:00.00'
description: 'La buena acción diaria, el estar siempre listo o Siempre listo para Servir; son cuestiones que tenemos claras en el concepto, y que tratamos de aplicar todos los días...'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "historias"
tags: ["historias", "libro", "scout", "no hubo dragones", "dragones", "catastrofe", "entrevista"]
img: 'https://i.postimg.cc/D0Fjg10B/image.png'
authors: ["Norberto Daniel Ceballos"]
---


{{< img src="https://i.postimg.cc/D0Fjg10B/image.png" >}}


La buena acción diaria, el estar siempre listo o Siempre listo para Servir; son cuestiones que tenemos claras en el concepto, y que tratamos de aplicar todos los días, pero… ¿Hay un límite para esto? Baden Powell, en “Escultismo para muchachos”, nos dice algo sobre el estar Siempre Listos:



> _“Significa que vosotros estaréis siempre preparados, tanto mental como corporalmente para cumplir vuestro deber. Siempre listos mentalmente mediante de la disciplina, siendo obediente a las órdenes que os den y habiendo recapacitado de antemano sobre cualquier accidente o situación que pueda ocurrir, de tal manera que podáis hacer lo adecuado en el momento preciso y tengáis la voluntad de ejecutarlo”_



Conociendo lo que nuestro fundador dijo sobre nuestro lema, toca ahora hablar de esas personas que lo llevaron consigo y dieron todo de sí por el bien de los demás: Los scouts de México durante el terremoto del 85´. Todo ocurrió frente a las costas de Michoacán, en el océano Pacífico. Su onda expansiva tardó dos minutos en llegar al centro del país. A las 7:19 horas del 19 de septiembre de 1985 la capital de México se sacudió con un sismo de magnitud 8,1. El movimiento devastó a la zona centro de la ciudad, provocó daños severos en cientos de edificios y causó la muerte de miles de personas.


{{< img src="https://i.postimg.cc/hvpr1wHt/image.png" >}}


Para entender un poco el contexto de sobre este acontecimiento, nos pusimos en contacto con un Scout de México, llamado Eduardo Gallardo Sánchez, que nos comentó un poco como se recuerda esa fecha en su país:



> _“Sobre el terremoto en general acá en México es un día muy recordado, fue un día muy duro, en especial para la Ciudad de México, cada año, el 19 de septiembre, se hace un simulacro conmemorativo en todo el país, y en la mayoría de lugares se dan unas palabras sobre ese día y se recuerda de lo importante que es el tiempo de respuesta de todos durante un sismo; en las escuelas, en los niveles inferiores (kínder, en especial primaria, secundaria) se enseña a los niños sobre ese terremoto, se dan platicas, se ponen documentales, incluso hay maestros que dan testimonios, yo recuerdo en la primaria, un maestro nos platicó que a él le toco cuando era joven, vivía en el DF, dijo que se estaba preparando para la escuela cuando empezó, afortunadamente no le paso nada pero que salió a la calle y muchos edificios se habían caído, ayudo y habló con personas que quedaron debajo de los escombros, y estuvo apoyando.”_



Pero… ¿Por qué nombramos a los Scout?, ¿Qué tiene que ver el Titulo “No hubo Dragones”? Este titulo del articulo corresponde a un libro con el mismo nombre que posee testimonios de scout que vivieron este acontecimiento y brindaron apoyo durante lo sucedido. Como anécdota personal, yo no lo conocía al libro, pero una persona especial para mí estaba intrigada sobre él, así que lo busque en internet y se lo regale. Unos meses después comencé a leerlo para ver de qué se trataba, y a medida que iba leyendo más testimonios, no podía creer lo que estaba escrito. Hablaban de chicos, desde la Manada hasta los Rovers que, al conocer la noticia, tomaron iniciativa de ponerse su uniforme e ir a donde había ocurrido todo y brindar su servicio para acciones como: Acordonar zonas, ayudar a los bomberos, sacar a los sobrevivientes de entre los escombros y muchísimas otras cosas en las que no dudaron ni un minuto para ofrecer su “Siempre Listo”.

El libro posee muchos testimonios que son impactantes pero que, como opinión personal, creo que muchos de los que estamos en el movimiento deberíamos leer una vez en la vida para tener en cuenta de lo que, como Scouts, podemos hacer por la sociedad. A continuación, voy a dejar algunos fragmentos, para que se pueda observar un fiel testimonio de la importancia del servicio sin esperar nada a cambio:


{{< img src="https://i.postimg.cc/cJ6f04kx/3785f353-c94c-463b-882b-b470c9405e5d.jpg" >}}

{{< img src="https://i.postimg.cc/K80t6gNp/53ce86fd-b48d-489e-84f4-ff81abe87ba0.jpg" >}}

{{< img src="https://i.postimg.cc/s287Xdbp/9b4c0235-28f4-4a26-ad82-14cea0a57784.jpg" >}}

{{< img src="https://i.postimg.cc/rwVSZ9St/6b21720a-dba1-41d4-a56b-0ebc798a8a8c.jpg" >}}


Aparte, Eduardo nos comentó un poco de cómo recuerdan, en los scouts, estos acontecimientos:



> _“Acerca de los scouts, esa fecha es conmemorada, y es importante porque muchos scouts ayudaron ese día, portando o no el uniforme, ayudaron recogiendo escombros, ayudando a personas afectadas, moviendo víveres, reconstruyendo construcciones dentro de lo posible, esos hechos se usan mucho como ejemplo del servir de un scout y muchos grupos dan palabras y hacen actividades conmemorando la fecha”_



Y, de paso, comento también sobre otro suceso importante que también ocurrió en México:



> _“De hecho, no sé si supiste, hace tres años, exactamente en la misma fecha del aniversario del 85´, el 19 de septiembre de 2017 ocurrió otro terremoto con epicentro en la ciudad de Puebla, a unos 200 kilómetros de la Ciudad de México, este segundo terremoto tuvo un impacto similar al del 85, varios edificios se cayeron, y hubo muchas personas afectadas al igual que en el primero, muchos scouts participamos ayudando, acá en mi estado, en el pueblo donde vivo, en las comunidades que rodean, muchas casas son de adobe, un material que ya no es tan común en las construcciones modernas, la mayoría de esas casas cayeron. Como scouts, organizamos para principalmente ir a levantar escombros, en la Ciudad de México, que de nuevo fue la más afectada, también se vio mucho la participación de los scouts, reflejando la enseñanza de las conmemoraciones de cada año hacia el terremoto del 85´. Actualmente son dos fechas que son recordadas en México, los terremotos del 85´y del 2017, ambos el 19 de septiembre, debido al impacto sobre la capital, a toda la gente afectada y a la solidaridad de la gente con las contingencias.”_



Nadie pide que salgamos ahora a sacrificar nuestras vidas para ayudar a los demás, pero siempre tengamos en cuenta ese lema de “Siempre Listos” y esa promesa que hicimos voluntariamente un día, y tratemos de hacer de cuanto nosotros dependa para dejar el mundo mejor de lo que lo encontramos…  



**¡Siempre Listo!**




## Fuentes



* https://www.bbc.com/mundo/noticias/2015/09/150917_mexico_sismo_antes_despues_fotos_an
* “No hubo Dragones – Testimonios Scouts del terremoto de 1985” https://www.scout.org/sites/default/files/scout_news/No_Hubo_Dragones.pdf
* Muchas Gracias a Eduardo Gallardo Sánchez por ayudarnos a construir este artículo.
