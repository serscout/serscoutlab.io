---
title: 'Lo que el viento se llevo - BP'
date: '2020-07-14 12:11:00.00'
description: 'Hace no mucho leí este artículo en una página de Facebook...'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "opiniones"
tags: ["opiniones", "escultismo", "bp", "fundador"]
img: 'https://i.postimg.cc/4yxhBVMn/image.png'
authors: ["Joaquin Decima"]
---

Hace no mucho leí este artículo en una página de Facebook, sin embargo en ese momento este blog andaba medio parado, pero hoy recordando un poco porque fue creado y para que decidí que era momento de compartir, tal como dije siempre la voz de los scout tiene que hacerse eco y ser llevado hasta donde pueda.




## Una crítica



En esta oportunidad la crítica no es netamente mía, (como muchos sabrán a mi me gusta ser abierto con mis pensamientos) Pero en este blog no solo existo yo, y en esta oportunidad quiero remarcar que no es solo para los autores que redactan aquí sino para toda voz que quiera ser escuchada.


{{< img src="https://i.postimg.cc/4yxhBVMn/image.png">}}


Lo que vamos a ver a continuación fue publicado el 23 de Junio por la página _“Scout Latinos”_ y hace referencia a los sucesos que ocurrían por esos días en latinoamerica…



> Ante todo, debo pedir disculpas, por si suelto alguna palabra mal sonante. Pero creedme que estoy un poco harto de lo que leo últimamente.
>
> Todos los scouts sabemos QUIEN FUE Baden Powell, su vida y milagros desde el campamento de Brownsea, hasta su fallecimiento en Kenya.
>
>Pero por lo que se lee por ahí, incluso en periódicos, es que B-P fue racista, homófobo, reprimió a los Ashanti, Matabeles, Zulúes y Afganos, fue partidario del Nacional Socialismo…y algunas capulladas más.
>
> Por este motivo, algunos “hermanos” scouts, (y escoltes, en Cataluña también hay bastantes) encuentran justificado que, en Coímbra, Portugal, la estatua de B-P se haya quedado sin cabeza.
>
> No sé si hay algún dirigente de alguna Federación Scout estatal, autonómica, o independiente que nos lea. Si es así, me permito, como antiguo scout, rover o scouter, decirle que por favor den a conocer entre la gente asociada, la historia de quien fue Baden Powell, y lo que es más importante QUÉ NO FUE. No sé si José María López Lacárcel, a través de tus libros, o Juan José Pérez Martínez, con tus artículos en la Roca del Consejo, podéis hacer que la gente sepa de qué habla, cuando opina y dice bobadas.
>
> Todos sabéis quien fue Von Ribbentrop. Fue un dirigente de la Alemania Nazi que fue ajusticiado después del Juicio de Núremberg. Este hombre, siendo embajador en Londres, se entrevistó con Baden Powell, el cual además mantuvo contacto con algún dirigente de las Juventudes Hitlerianas, entre otras cosas para conseguir que los nazis no prohibieran el escultismo en Alemania, cosa que evidentemente pasó. Pues bien, este hecho, es utilizado por algunos para afirmar que B-P era pro Nazi.
>
> La gente habla sin tener ni puta idea de lo que habla.
>
> Vosotros mismos, nos va en ello el prestigio de nuestro/vuestro escultismo y de su fundador.
Un abrazo, buena verbena, y felicidades a los “juanes”



Así tal cual nos invita a **refleccionar** uno de los miembros del staff de esta página de Facebook. Se que como scouts evitamos el uso de malas palabras entre otras cosas, sin embargo en esta ocacion decidi literalizar lo que nuestro compañero tenía para decirnos. Muchas veces un insulto hace que se pierda el verdadero mensaje pero en este caso no lo veo así.

Simplemente veo (y comparto) la indignación que genera cuando la gente habla desde el desconocimiento menospreciando sin fundamentos reales a algo que seguimos fielmente millones de personas, no como doctrina, sino como forma de vida…



¿Vos que pensas? No te olvides que **siempre te leemos en los comentarios**!


