---
title: 'El Scout Desconocido'
date: '2019-07-14'
description: 'Muchos de nosotros conocemos la historia del scout desconocido. Una historia que nos explica que la hermandad scout es mas grande que cualquier cosa'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "historias"
tags: ["scout", "desconocido", "historias"]
img: 'https://i.postimg.cc/xCD070G9/elscoutdesconocido.png'
authors: ["Joaquin Decima"]
---

Hay una historia que siempre se ha contado en los scouts, y que tiene ese halo de irrealidad que hace que muchas veces tengamos dudas sobre si la historia es real o simple leyenda.

Cuenta esta historia, que en plena Segunda Guerra Mundial, un soldado americano se hallaba herido de gravedad en territorio enemigo. De pronto vio llegar a un soldado enemigo, japonés, armado, que le apuntó directamente al corazón.

{{< img src="https://i.postimg.cc/RZSG7fjB/elscoutdesconocido3.jpg" >}}

El soldado americano, seguro de que iba a morir, hizo con un último esfuerzo el saludo Scout, que era para él el recuerdo de una de las mejores épocas de su vida, frente al enemigo. Después perdió el conocimiento.

Al abrir de nuevo los ojos, vio que alguien le había vendado las heridas, y encontró junto a él una nota:

“Esta guerra nos ha hecho enemigos, y mi primera intención al verte fue matarte por ser una amenaza para mi país. Pero al ver tu saludo scout, no he podido apretar el gatillo, ya que yo también fui scout en mi juventud, y nunca podría asesinar a un hermano.”

El estadounidense nunca volvió a saber de aquel hombre, pero gracias a los cuidados que le proporcionó, pudo volver a su país y sobrevivir a aquella guerra.




Lo que poca gente sabe, es que esta historia no sólo es cierta, sino que tiene hasta un monumento conmemorativo. Os pego (desde La Roca del Consejo) el testimonio que lo demuestra:

En 1951, Sr. Mishima, entonces jefe Scout de la Asociación de Boy scout de Japón (ahora Scout Association of Japan), estaba asistiendo a una Conferencia Mundial realizada en EE.UU. Durante la misma pudo visitar la Oficina Central Nacional en la cual le narraron la siguiente historia:

{{< img src="https://i.postimg.cc/ncY09TS2/elscoutdesconocido2.jpg" >}}

Esta es una historia verdadera de la Guerra del Pacífico en la selva de una isla del Pacífico Sur, justo antes del final de la Segunda Guerra Mundial.

Durante la batalla decisiva entre los soldados japoneses y estadounidenses, un soldado americano malherido, tendido sobre la tierra, notó el sonido de alguien viniendo en su camino. Al abrir sus ojos, vio a un soldado japonés precipitarse hacia él con una bayoneta. El soldado americano, herido e incapaz de moverse, considerándose ya hombre muerto se desmayó.

Al cabo de un rato, despertó y el soldado japonés se había ido. Cerca de él un pequeño papel le llamó la atención y lo guardó. Después de ser rescatado y ya descansando en el hospital, recordó el pequeño papel y se lo dió a un médico para solicitar una traducción, resultando el texto siguiente:

“Yo soy el soldado japonés que intentó matarte con la bayoneta, te vi saludar con tres dedos y recordé que yo fui también scout. Los Boy scouts son hermanos, y es imperdonable matar a un soldado herido, te he dado primeros auxilios, buena suerte”

Seriamente herido el soldado volvió a casa, y con su padre visitó los BSA (Boy Scouts of America), narrándoles esta historia y dejando un donativo.
Sr. Mishima volvió a Japón y trató de localizar a este soldado japonés, pero no pudo encontrarlo. Es muy probable que muriese en combate.

{{< img src="https://i.postimg.cc/xCD070G9/elscoutdesconocido.png" >}}

De la historia “del soldado desconocido” se ha hablado por mucho tiempo, tanto es así que existe un monumento en conmemoración en Kodomo no kuni (parque “Mundo de los niños”) cerca de Yokohama, Kanagawa Prefectura, Japón.

Esta publicacion fue tomada de {{< textlink text="Facebook" url="https://www.facebook.com/GSParaguana/photos/gm.2443401075718387/348926042454946/?type=3&theater&ifg=1"  >}} Se agradece a todos los hermanos scouts por sus aportes!
