---
title: 'La batalla de los egos'
date: '2021-03-09 12:36:00'
description: 'Para hablar sobre el tema quiero tocar en este artículo, vamos a ponernos en una situación en la que muchos hemos participado'
type: 'publicaciones'
categories: ["raiders","tropa","rovers","manada"]
color: "universal"
theme: 'opiniones'
tags: ["ego", "egos", "batalla", "educadores", "dirigentes", "raiders", "unidad", "tropa", "reuniones", "consejo", "grupo", "zona"]
img: 'https://mauriciocohensalama.com/wp-content/uploads/2019/04/Calmar-el-ego-1030x593.jpg'
authors: ["Norberto Daniel Ceballos"]
---

{{< img src="http://puntodepartidatv.com/wp-content/uploads/2021/01/Vin-Diesel-y-Dwayne-The-Rock-Johnson.jpg" >}}


Cuantas personas conocemos, que, a través de su personalidad, nos muestran el ego que cargan. De este tipo de personajes encontramos en todos los ámbitos en los que convivimos: la escuela, la facultad, el trabajo, entre otros; y hasta nos acostumbramos a ellos. De hecho, muchos saben que necesitan de esto para poder crecer en sus ámbitos, desde políticos, hasta boxeadores y pasando también por raperos, utilizan su ego para poder hacer crecer su figura dentro de sus áreas. Pero, ¿qué pasa cuando crece este aspecto negativo dentro de un movimiento que enseña a los jóvenes a ser agentes de cambio para el futuro?

Cuando sos beneficiario, lo que te importa es vivir el movimiento y cada una de las actividades que te presentan los dirigentes. Cada sábado es una aventura nueva que se quiere disfrutar al máximo, poco te interesa las cosas que se pueden hablar en un consejo de grupo o siquiera ignoras que es un consejo de grupo.

En algunos casos, a medida que vas pasando entre las etapas y vas llegando a las ramas mayores (Caminantes o Rovers), te vas interiorizando un poco como son los organismos internos de tu asociación. De hecho, dentro de la asociación en la que estoy como voluntario (Scout de Argentina), se dan espacios de participación para que los jóvenes se involucren en las decisiones que se toman a futuro para la asociación.


{{< img src="https://www.infobae.com/new-resizer/fcP7FGDKgv5wzZhC_ujLQwrpKuw=/768x432/filters:format(jpg):quality(85)/cloudfront-us-east-1.images.arcpublishing.com/infobae/Q62YUSMDXVHAFJIU3I36M7C6YM.jpg" >}}


Lo que ocurre, es que a medida que te vas sumergiendo en todos los niveles de tu asociación, (grupo, distrito, zona, nación) vas conociendo a estos personajes que, con su ego, quieren arrasar con todo y con todos, tratando de imponer sus ideas y esperando a que todo el mundo le dé la razón solamente por ser él y sin ningún derecho a réplica.

Pero, para poder continuar con lo que se quiere expresar en este artículo, primero debemos definir que es el ego. No nos centraremos en analizar a esta palabra desde el punto de vista del psicoanálisis, sino que buscamos un significado más cotidiano. Según la Real Academia española, el ego es:



>“**En el vocabulario coloquial**, ego puede designar el **exceso de valoración** que alguien tiene de sí mismo. Como tal, es sinónimo de inmodestia, arrogancia, presunción o soberbia.”
>


{{< img src="https://mauriciocohensalama.com/wp-content/uploads/2019/04/Calmar-el-ego-1030x593.jpg" >}}


Al leer esta definición, posiblemente algún nombre dentro del movimiento scout se nos pueden venir a la cabeza, y lo inquietante, es que se puede encontrar desde las personas que ocupan los altos cargos, hasta un dirigente en un consejo de grupo; son estos personajes que pueden complicar hasta una mínima reunión.

No se tiene que confundir el termino de **“ego”**, con la confianza o la seguridad que uno puede tener en si mismo o en las ideas que tiene. Recordemos que uno de los pilares fundamentales del escultismo es el desarrollo de los niños, niñas y jóvenes a través de la autosuperación y el compromiso a la idea de ser agentes de cambio para dejar un mundo mejor. Para ello, necesitamos convicción de lo que estamos realizando, pero cuando nos encerramos en solamente ocuparnos de aquello que es para nuestro propio interés y beneficio, sin atender ni reparar en las necesidades del resto, es donde estamos fallando a la promesa a la que alguna vez nos adherimos voluntariamente. De hecho, muchas veces se ve como las discusiones que pueden tener algunos dirigentes, tanto en persona como en las redes sociales, lejos están de ver cómo lograr una mejor educación no formal para los beneficiarios, cerca de la naturaleza o con mejores actividades, sino, que cada vez tienen más tintes políticos.

Y, aunque esto pueda ser una opinión personal, no hay que dejar de lado lo que Baden Powell, fundador del movimiento el cual promulgamos, pensaba sobre esto:



>“La individualidad demasiado desarrollada, significa dejar suelto al egoísmo que es lo contrario de lo que nosotros deseamos”
>
> {{< citaname name="Roverismo hacia el éxito" >}}  



Por último, desde nuestro lado, y como finalidad de este artículo, te queremos dar una ayuda para poder trabajar con este tipo de personas, para lograr un ambiente de trabajo optimo en donde todos podamos pensar en lo importante: que es lo mejor para nuestros chicos, así que… ¡Toma nota!



* A la hora de debatir, dejar en claro que no solo hay una verdad, porque puede ocurrir la famosa frase: “solo yo tengo razón, el otro está equivocado”.
* Una regla de oro en cualquier reunión es centrarse en los temas y nunca en las personas. Si se convierte en un ataque personal lo mejor es cerrar el diálogo para evitar en todo momento los malentendidos.
* No usar la fuerza del cargo que ocupas, ya seas jefe de rama o de grupo, para hacer ver que el otro esta equivocado y mantener una postura por no dar el brazo a torcer.
* Pone de tu parte para que la convivencia sea lo más cordial posible. Un scout es generoso, cortés y solidario.
* No compitas contra esa persona, evita entrar en ese juego de compararte con otros.
* Mentalízate antes de mantener contacto con una persona prepotente, intenta hacerte la idea sobre ello y definir claramente aquello que quieres expresar o conseguir de la otra persona. Este entrenamiento o preparación previa puede ser de ayuda para mostrarte de una manera sólida.
* Siempre analiza la situación, si sientes que sus comentario o actitudes te afectan, intenta analizar objetiva y racionalmente para que no altere tu estado de ánimo.
* Defiende tu postura de forma asertiva y procurando alterarte lo menos posible, es importante que defiendas tus puntos de vista y no te dejes someter por una persona con un gran ego.



Estos son solo de algunos de los consejos que te podemos dar para trabajar con este tipo de personas, (pueden existir muchísimas más). Si vos conoces y te gustaría compartir algún otro consejo, te leemos en los comentarios y entre todos, vayamos compartiendo nuestras experiencias, y recordemos que "Si tenéis el hábito de tomar las cosas con alegría, rara vez os encontraréis en circunstancias difíciles."



{{< citaname name="¡Siempre Listos!" >}}
