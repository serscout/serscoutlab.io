---
title: 'El camino del educador'
date: '2020-08-06 10:58:00.00'
description: 'El Scultismo tiene educacores y hoy vamos a ver mi opinion sobre la responsabilidad de ser educador'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "opiniones"
tags: ["opiniones", "escultismo", "educadores", "dirigentes", "educador", "dirigente", "malo", "padres"]
img: 'https://i.pinimg.com/originals/b6/79/65/b679658f1278a962a50d5ffc6b24a4ff.jpg'
authors: ["Joaquin Decima"]
---



En esta clase de opiniones no suelo ser muy “bueno” y puedo llegar a ser un tanto “duro” con algunas palabras. Por mucho tiempo vi cómo pasan cosas que desvirtúan el escultismo volviendo a muchos grupos a cosas similares a los *Club de Barrio.*

{{< img src="https://saludpublicayotrasdudas.files.wordpress.com/2018/05/img_0620.jpg" >}}

En los grupos que participe como educador la ausencia de los mismos era amplia. Siempre faltaban educadores, más específicamente “jefes de ramas” que son los educadores que llevan adelante el plan de formación de los jóvenes.




## Donde soy el malo



{{< img src="https://i.pinimg.com/originals/b6/79/65/b679658f1278a962a50d5ffc6b24a4ff.jpg" >}}


Si hay algo que tenemos los hermanos scouts es que somos sinceros y decimos la verdad. Nosotros podemos amar el escultismo sin importar la edad que tengamos y como tal nunca es tarde para formar parte del mismo, sin embargo, siempre debemos ser honestos con uno mismo y saber hasta dónde puedo cumplir un compromiso. Muchos grupos scouts caen en obtener dirigentes que no saben medirse, ya sea por orgullo o por desconocimiento.




## Los papdres como educadores


{{< img src="https://estaticos.serpadres.es/media/cache/1140x_thumb/uploads/images/article/56f130b55cafe84998a84bfe/padrestoxicos_0.jpg" >}}


Cuando escribo esto me da un poco de miedo a que lo tomen a mal, pero es mi pensamiento y es por respeto propio creo que es importante ser sincero… No voy a decir que un padre que quiera colaborar deba ser rechazado, eso nunca… Yo me opongo a los padres que después de unos cursos se los asigna como jefes de rama y ahora quiero explicar porque…




## El escultismo es Aprender haciendo



Los scouts aprendemos haciendo, más allá de los libros, los cursos, debemos tener experiencia y yo vivi tener dirigentes que lejos de transmitir el escultismo parecía una clase de confirmación… Pasamos horas sentados hablando de Dios, no digo que esto esté mal, una vez, pero que todos los sabados sean horas de dios y poco escultismo simplemente aleja… Más allá de que yo no crea en un dios (dado que en el budismo no existen Dioses), los jóvenes deben encontrar a dios en el movimiento no en horas sentados hablando de eso, Los mejores scouts te muestran a dios en pleno juego y no en una lectura bíblica…

Ya anteriormente nuestro colega hablo de {{< textlink url="/posts/2020/06/aprender-haciendo-la-raz%C3%B3n-del-escultismo/" text="aprender haciendo" >}}, y creo que todos aquellos que fuimos scouts desde jóvenes entendemos que es la mejor forma de aprender y de generar nuevos conocimientos, la experiencias son muy enriquecedoras en este sentido.




## Mi postulado



Claramente me siento muy feliz cuando un padre encuentra que el escultismo es lo que quiere vivir en su vida, se arrepiente de no conocerlo antes y ante esto siempre lo incluyo en las actividades. Siempre hay que buscarle una rama en la que se sienta cómodo, incluirlo en las actividades y demás para que entienda nuestro mundo, cuando se suelte debería comenzar a dirigir alguna que otra actividad y estar siempre activamente en las actividades de los jóvenes.


{{< img src="https://www.lancetalent.com/blog/wp-content/uploads/19425-NS6SU2.png" >}}


Pero de ahí a formular promesa, o hacerse cargo de una rama hay un gran camino que debe realizar junto a otros MS. Ni un curso, ni un taco de madera te habilitan a educar a los jóvenes. Uno puede responsabilizarse con la rama cuando efectivamente entiende el juego y eso puede pasar en un año, en 5 años o nunca. Y puede que alguno nunca se haga cargo de una rama, pero eso no le impide colaborar.




## La contracara


{{< img src="https://i.pinimg.com/originals/6b/55/b7/6b55b7795065d0b5d8d6de9b8a51396b.jpg" >}}


Con esto pensarían que todos los que venimos desde jóvenes podemos hacernos cargo de las ramas y esto tampoco es correcto. La consigna es exactamente la misma, no existe curso, ni taco de madera que te habilite a guiar a tus hermanos scouts, muchos jóvenes viven todas las etapas jugando al juego scout sin terminar de comprenderlo, y esas personas pueden ayudar mucho al grupo, al igual que un padre, pero la decisión de quienes se responsabilizan por guiar a los jóvenes debe ser de aquellos que comprenden el juego scout y pueden transmitirlo naturalmente




## ¿Que debo hacer?



Como educador siempre debe tener un feedback de los jóvenes. Y como tal debo responsabilizarme de esto. Comprender lo que pasa y modificar mis metodos para que los jóvenes se sientan cómodos y aprendan. El escultismo es para ellos y si nos cumplimos con eso es porque no lo entendimos...
