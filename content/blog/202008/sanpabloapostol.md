---
title: 'Patronos: San Pablo Apostol'
date: '2020-08-03 14:14:00.00'
description: '¿Alguna vez nos preguntamos por qué para cada rama se eligió un patrono o patronos en particular? hoy San Pablo Apostol '
type: 'publicaciones'
categories: ["rovers"]
color: "rover"
theme: 'historias'
tags: ["historias", "pablo", "apostol", "patronos", "rovers", "tarso"]
img: 'https://i.postimg.cc/NfMmZkvP/image.png'
authors: ["Norberto Daniel Ceballos"]
---

Cuando uno lee las palabras que B.P escribió para los rovers en el libro *“Roverismo hacia el éxito”*, queda muy claro el mensaje para aquel que transite esta etapa:



> “Resuelve los escollos al ir por el ´rio de la vida´ remando tu propia canoa y siempre, encaminad vuestros pasos a aquella estrella que se ve en el firmamento, ósea, apuntad alto”


{{< img src="https://i.postimg.cc/NfMmZkvP/image.png" >}}


Pero para un Rover que recién ingreso en la rama y se le muestra que San Pablo es el patrono que simboliza todo eso que se describe en la oración que dejo Baden Powell, se puede preguntar: ¿Qué fue de la vida de este santo y por qué fue elegido como patrono de los Rovers? Bueno, en esta publicación vamos a hablar un poco de él y entender porque su vida fue un ejemplo de servicio sin esperar nada a cambio.




## Pablo de Tarso



Pablo habría nacido entre los años 5 y 10 D.C (no se sabe específicamente cuando), en la ciudad de Tarso, que ocupa el territorio que hoy pertenece a Turquía. Nació en el seno de una familia acomodada de artesanos judíos y paso su infancia, hasta los 13 años, frecuentando una escuela elemental en el ámbito judío, donde aprendió el idioma griego y todo sobre la Biblia en lengua griega.

Habiéndose trasladado de adolescente a Jerusalén, Pablo asistió a las clases del gran rabino Gamaliel, el Viejo. Aquí adquirió un profundo conocimiento de la Torá según la escuela de los fariseos y, además, contaba con una sólida formación académica que incluía conocimientos sobre teología, filosofía, hechos jurídicos, lingüística y mercantil; asimismo, también hablaba perfectamente los idiomas: latín, arameo y hebreo.


{{< img src="https://ichef.bbci.co.uk/news/ws/410/amz/worldservice/live/assets/images/2014/11/21/141121072503_sp_st_paul_the_apostle_624x351_thinkstock.jpg" >}}


En concordancia con la educación que había recibido, se proclamó por aquellos años como acérrimo perseguidor del cristianismo, considerado entonces una secta herética del judaísmo, del cual se escuchaban rumores de “blasfemias contra Moisés y contra Dios”, o sea, contra la Ley y el templo de aquel momento.

Su actividad persecutoria se extendía de Jerusalén a Damasco, específicamente contra el grupo judeo-helenístico del diacono Esteban y sus compañeros, los cuales fueron lapidados. Pero, en un giro inesperado, sufrió el vuelco de su vida: **“fue alcanzado por Cristo”**. Los jefes de los sacerdotes de Israel le confiaron la misión de buscar y hacer detener a los partidarios de Jesús en Damasco. Pero de camino a esta ciudad, Pablo fue objeto, de un modo inesperado, de una manifestación prodigiosa del poder divino: deslumbrado por una misteriosa luz, arrojado a tierra y cegado. Luego, el mismo Jesús se le apareció, le reprochó su conducta y lo llamó a convertirse en el apóstol de los gentiles (es decir, de los no judíos) y a predicar entre ellos su palabra. Tras una estancia en el pueblo (donde, después de haber recuperado la vista, se puso en contacto con el pequeño núcleo de seguidores de la nueva religión), se retiró algunos meses al desierto (no se sabe exactamente dónde), haciendo así más firmes y profundos, en el silencio y la soledad, los cimientos de su creencia.


{{< img src="https://upload.wikimedia.org/wikipedia/commons/7/7d/ElGrecoPaul_%28cropped%29.jpg" >}}


Vuelto a Damasco, en el año 39, hubo de abandonar clandestinamente la ciudad y aprovechó la ocasión para marchar a Jerusalén y ponerse en contacto con los jefes de la Iglesia, San Pedro y los demás apóstoles; para poder encomendar todas las energías al servicio de Jesucristo y del evangelio. Se dedico a predicar con ardor misionero en las sinagogas, desde Chipre hasta Anatolia, contando la Buena Nueva de la resurrección y de la salvación en Jesús; fundo comunidades y curo a muchos enfermos. El esfuerzo realizado por Pablo de Tarso en sus viajes es digno de mención: viajó por diversas partes del mundo como Grecia, Asia Menor, Siria, Palestina, entre otros, y también, escribió cartas a diferentes pueblos del mediterráneo.
Las cartas de San Pablo fueron los primeros documentos del Nuevo Testamento, escrito mucho antes que los evangelios. Las cartas paulinas nos ofrecen una visión interna de los desafíos de los primeros seguidores de Jesús.

En una de sus vueltas a Jerusalén, fue encarcelado por el quiliarca Lisia, quien lo envió al procónsul romano Félix de Cesarea. Allí pasó el apóstol dos años bajo custodia militar. Decidieron embarcarlo con destino a Roma, donde los tribunales de Nerón decidirían sobre él. De los años 61 a 63 D.C vivió en Roma, parte en una prisión y parte en una especie de libertad condicional y vigilada, en una casa particular. Luego, fue puesto en libertad, ya que los tribunales imperiales no habían considerado consistente ninguna de las acusaciones hechas contra él.

En el año 66, San Pablo fue nuevamente detenido y encerrado en una horrenda cárcel, hasta que fue condenado a muerte: según la tradición, como era ciudadano romano, fue decapitado con la espada.



---



Como se dijo en el artículo referido a la vida de San Francisco Javier, uno puede profesar la fe que sienta desde su corazón, o puede que todavía este en búsqueda, o simplemente sentir que la fe no es necesaria en su vida; pero en lo que todo podemos coincidir es que San Pablo de Tarso fue un ejemplo de devoción hacia sus ideales, eligió el camino, la ruta, el sendero difícil de vivir de acuerdo a unos valores, remo esa canoa que el eligió, y a pesar de los escollos que pudo tener durante todo el momento que predico la palabra de Jesús, nunca bajo sus ideales y siguió… **¡APUNTANDO ALTO!**



*¡Siempre Listos!*
