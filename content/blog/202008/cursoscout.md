---
title: 'Mi experiencia en los cursos'
date: '2020-08-21 17:34:00.00'
description: 'Mi experiencia con respecto al rol del educador'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "opiniones"
tags: ["opiniones", "escultismo", "educadores", "dirigentes", "educador", "dirigente", "malo", "padres", "violencia"]
img: 'https://2.bp.blogspot.com/-zFauUWiZX8k/USEWhIIHUQI/AAAAAAAAAkY/01n7HtLEFzY/s1600/imagedfffffs.jpeg'
authors: ["Joaquin Decima"]
---

Con anterioridad hable de los cursos sobre escultismo. Y muchos saben que no estoy de acuerdo con que un curso te de una insignia que signifique que entiendes el juego scout, vi muchos que simplemente llevan colgados taquitos de maderas pero predican clubes de barrios o cosas muy alejadas del escutismo.




## Al Cesar…



*Al Cesar lo que es del Cesar* una frase que utilizo muy a menudo. Y siempre creo que hay que contar las dos caras de la moneda cuando las hay y en esta oportunidad vamos a hablar de los cursos en sí. Y en esta oportunidad quiero explicar que esta mal y que esta bien desde mi punto de vista…




## Bien y Mal



A continuacion dejo este especie de lista que intenta explicar que lo que me parece bien es la formación y que fomenten la misma. Lo que molesta es que te den una insignia y que la gente se crea el Scout del año por tener esa insignia.



* Tener cursos que nos ayuden a mejorar nuestro desempeño como educadores. **bien**
* Que obtengas una insignia de madera por esto. **mal**
* Que te permita profesionalizarse. **bien**
* Que una insignia “demuestre” cuanto sabes. **mal**




## Mi experiencia



Yo me encuentro en SAAC hace ya 2 años y no puedo evitar tener que ir a los cursos, por lo cual este año atípico retome los cursos online. Y como todo tengo múltiples experiencias pero en este caso quiero compartir una hermosa experiencia que tuve con un grupo de trabajo.


{{< youtube code="iyBzkKt11jU" >}}


Espero que entiendas el mensaje que se intentó comunicar y no pierdas la oportunidad de mostrar que es lo que haces con tu grupo!
