---
title: 'La historia de nuestra insignia'
date: '2020-08-14 10:49:00.00'
description: '¿Alguna vez nos preguntamos por qué para cada rama se eligió un patrono o patronos en particular? hoy San Pablo Apostol '
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: 'historias'
tags: ["lis", "flor", "scout", "historias", "insignia"]
img: 'https://lh3.googleusercontent.com/proxy/DdWWhhJ3UR6jQwSISgLYLC_qXzrYBevx7NOAFK-dRhOaJeptAKubRbExMbFuEY06j9uGzsNE5ZDBcdxbPcQIJD31mKpbOzpuPdA'
authors: ["Norberto Daniel Ceballos"]
---

Entre todos los símbolos que tenemos los scouts como el lema, el saludo o la ley; el que más nos representa en todo el mundo es nuestra insignia característica: Ese color morado con la flor de lis en el centro y con el nudo llano que lo rodea. Un símbolo que nos entregan cuando uno decide firmemente formular esa promesa que nos va a acompañar toda la vida, pero… ¿Conocemos su historia? ¿Cuál de los muchos significados que se le pueden otorgar son correctos? ¿Por qué usar ese tipo de flor? Bueno, en esta publicación vamos a tratar de desarrollar un poco toda su historia.


{{< img src="https://i.pinimg.com/originals/1d/93/7d/1d937db32749d8661f098fc2215f5b3b.png" >}}


Para responder algunas de estas preguntas, la mejor fuente que podemos obtener es a través de las palabras de nuestro fundador, Baden Powell, que escribió en el libro “Escultismo para muchachos”, donde nos da una explicación:



> “La Insignia Scout es una flor de lis que se usa en los mapas y en las brújulas para señalar el norte. Se ha escogido como insignia de los Scouts porque señala la dirección hacia lo alto, marca el camino que hay que seguir para cumplir con el deber y ser útil a los semejantes”



Esto nos indica que la flor de lis, es un símbolo que se utilizaba con anterioridad en otros ambientes que no eran el movimiento scout, pero ¿Dónde se utilizaban? Bueno, en esas brújulas o cartografías que nombra B.P se utilizaba un tipo especial de esta flor: se trata de una representación de la flor del lirio en heráldica. La heráldica es la ciencia del blasón, es decir, de las representaciones que aparecen en los escudos (tanto medievales como de otras épocas). Este símbolo, es una de las cuatro figuras más populares de la heráldica, junto con la cruz, el águila y el león, y se suele representar en color amarillo sobre un fondo azul. Pueden encontrarse varios tipos de flores de lirio, ya que existen de varios colores como Blanca, Amarilla, violetas, entre otras.


{{< img src="https://lh3.googleusercontent.com/proxy/mejPujSIBHo0sijgQrjMnL-fORfJEjnKSIqYxpvlYcp_lt7HVSDelt4VzagzsUD4fKduwR1zyxo_0Fyd3w5eRtXwuJUpKQknkUA" >}}

{{< img src="https://theoriginalgarden.com/Argazkiak/Fotos/Peques/IRISPSEUDACORUSLirioamarillo_1.jpg" >}}


La flor de lirio blanca representa virginidad, inocencia y pureza. También representa a la luz, elemento imprescindible en la religión cristiana. Además, su forma con tres pétalos unidos evoca a la religión cristiana, con la Santísima Trinidad. Por eso, este símbolo queda muy ligado a la Iglesia católica, y con ello, a la realeza, dos entidades fuertemente ligadas a lo largo de gran parte de la historia europea. 
También se considera un símbolo propio de la realeza francesa desde la Edad Media, cuando la empezó a usar Luis VII. En los años posteriores continuó apareciendo en los blasones de las dinastías, normalmente en forma de campo. Para ellos representaba el honor, el poder y la soberanía. La flor de lis fue parte del escudo de armas de la nación francesa hasta 1831. 


{{< img src="https://esacademic.com/pictures/eswiki/66/Blason_France_moderne.svg" >}}


En la cartografías o en las brújulas que se nombran anteriormente, esta flor tiene una constancia en la utilización del dibujo como parte de la rosa de los vientos, que indicaba los puntos cardinales en las cartas de navegación (mapas), apuntando hacia el norte.
Durante el siglo XX este símbolo fue adoptado por nuestro movimiento y, en la mayoría de los casos, se utiliza para distinguir la pertenencia de un país a otro. En ocasiones se sobrepone un símbolo nacional a la flor de lis y si ese país se compone de una federación de varias asociaciones, cada una hace lo propio para ser identificada.

Descubriendo cual es el origen de la flor que se encuentra en la insignia, veamos un poco más de los significados ocultos dentro de la misma. Pero antes, hay que recalcar algo importante, se podría decir que existen **dos Flores de Lis**: Una es la que aparece en el libro de *“Escultismo para muchachos”*, que contiene algunos elementos diferentes a la que actualmente conocemos. En esta, se puede observar la flor con los tres pétalos que nos recuerdan las tres partes de la promesa Scout, junto con la línea que divide al pétalo central que simboliza al scout capaz de seguir el camino recto en su vida, y las dos estrellas que cada punta representa a uno de los 10 artículos de la ley scout; pero se puede ver algunas diferencias, ya que debajo de esto se observa una voluta con la inscripción ¡Siempre Listo! (Be prepared! En el idioma ingles), y sus puntas están vueltas hacia arriba, como los labios del Scout que sonríe cuando, diligente, cumple con su deber. Unido a todo esto, se puede ver un cordón con un nudo en la punta para recordar la Buena Acción diaria.

En la flor que vemos hoy en día en muchos uniformes, podemos ver algunas modificaciones y algunos significados nuevos como, por ejemplo, el anillo que abraza los pétalos representa la unión y la hermandad mundial. Se puede ver una cuerda que rodea la flor y que se anuda en el centro, lo que le da la forma final al emblema. Este elemento nos recuerda que hay que ser constante y dedicar mucho esfuerzo y sacrificio para comprobar como cada día nos superamos y somos mejores que el día anterior. El nudo representa también la unión y hermandad mundial y el aprender haciendo.

Nuestro movimiento es algo tan grande y tiene tantas interpretaciones, que muchas veces nos podemos desviar un poco de lo que originalmente se pensó, pero acá, lo importante no es si falto algún significado a nuestro emblema o si había información de más, lo importante es recordar esa fecha a la cual prometimos fielmente cumplir con un estilo de vida, que alguna vez un dirigente me dijo que era como una segunda fecha de nacimiento, y cumplir nuestro deber para dejar el mundo mejor de lo que lo encontramos.



**¡Siempre Listo!**
