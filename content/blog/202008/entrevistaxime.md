---
title: 'El Movimiento Scout y las personas con discapacidad'
date: '2020-08-11 10:15:00.00'
description: 'En esta oportunidad vamos a entrevistar a Ximena Temperley una gran activista en la inclusión dentro de SAAC'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "entrevista"
tags: ["entrevista", "escultismo", "movimiento", "scout", "discapacidad", "ximena", "temperley", "saac"]
img: 'https://www.scout.es/wp-content/uploads/2015/12/discapacidad1.jpg'
authors: ["Joaquin Decima"]
---



En esta ocasión tuve la grata oportunidad de “entrevistar” a Ximena Garcia Temperley. La jefa del Grupo Scout Indios Kilmes. Y también un miembro importante de la comisión que impulsa una a la integración de todas las personas en el movimiento Scout (En este caso bajo SAAC)




## Sobre Ximena



* **Grupo Scout:** Indios Kilmes
* **Cargo:** Jefa de Grupo
* **Asociación:** SAAC
* **Nacionalidad:** Argentina
* **Rama Actual:** Rober
* **Motivo de la Entrevista:** El Movimiento Scout y las personas con discapacidad.
* **Especialidad:** LSA (Lengua de Señas Argentina)



Ximena aparte de ser jefa de grupo y excelente dirigente cumplio la funcion de intérprete en muchos comunicados de SAAC. Su experiencia formal y empírica la vuelven una persona muy respetable y querida en el ámbito. Todos conocen sus esfuerzos por la inclusión y su labor diaria.




## Entrevista



En el contexto actual de la cuarentena no pudimos “innovar” impulsando las entrevistas como videos en los patios de actividad o en lugares públicos. Sin embargo no queremos dejar pasar la oportunidad de tener una primer entrevista en la cual se nos permita conocer un poco más sobre este proyecto:




#### ¿Por qué iniciaron este proyecto?



> El por qué iniciamos el proyecto tiene que ver con las necesidades que se ven desde siempre en cuanto a la accesibilidad para las personas con discapacidad.  Y después de muchos años de trabajar cada uno haciendo un poco desde su lugar, pudimos formar un equipo gracias a una propuesta de la dirección de educación social de SAAC.




#### ¿Quiénes forman parte de este proyecto?



> Somos 7 personas aproximadamente que formamos parte del equipo, 4 trabajamos desde el 2019, y en marzo de este año se sumó más gente de distintas partes del país y hubo un nombramiento oficial desde la asociación.  Somos profesionales relacionados a la discapacidad,  desde profesores de Educación Especial, hasta asistentes sociales, abogados, etc.




#### Si tuvieras que resumirlo ¿Que nos contarías?



> En resumen trabajamos tratando de coordinar el tema a nivel nacional, para igualar criterios cuando hablamos de discapacidad y así evitar que se desvirtúe el tema desde paradigmas viejos.




#### Algun consejo para otras asociaciones ¿Que les dirias?



> Mi consejo para otras asociaciones sería que se trabaje siempre desde la Convención de las Personas con discapacidad,  la idea del Diseño Universal y el modelo Social de la Discapacidad.  Siempre formando equipos interdisciplinarios




## ¡Muchas gracias!



Desde ya queremos agradecer enormemente a Xime y a todo el equipo por la labor que realizan y el nivel de compromiso que tienen. Ya que sabemos que es importante en el movimiento unir criterios y “tirar todos para el mismo lado”.



En cuanto a vos contanos, ¿Cómo realizan este trabajo en tu grupo, asociación o país? ¿Qué propuesta harías vos?
