---
title: 'Historia del Guidismo'
date: '2019-09-27'
description: 'Se llama guidismo en castellano a la práctica del movimiento juvenil creado por Robert Baden-Powell y Agnes Baden-Powell en 1909 dirigido a niñas y jóvenes mujeres. El Guidismo se practica en 155 países.'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "historias"
tags: ["guidismo", "guias", "historias"]
img: 'https://blog.larocadelconsejo.net/wp-content/uploads/2014/03/1worldtrefoil1.jpg'
authors: ["Joaquin Decima"]
---

El sábado 21/09/2019 tuvimos la Hermosa oportunidad de conocer a un grupo de guías. Para algunos del movimiento scout esto les resulto raro pero emocionante. Pudimos aprender de su historia, informalmente, y de sus ideales, nos propusimos a estar en contra de los grupos que quieren absorber el guidismo y como tal creemos que es importante dar a conocer su historia.

## Historia

En el primer desfile Scout, en el Palacio de Cristal en 1909, Baden Powell se encontró con una Patrulla de niñas vestidas de scouts. Al preguntarles, su Guía de patrulla le respondió con aplomo que ellas eran las Chicas Scouts en clara alusión al término que se usaba para los primeros Scouts.

La presencia y el evidente ingenio de estas niñas hizo que BP cayera en la cuenta de que la aplicación del método del escultismo era una invitación abierta para el desarrollo personal y la formación del carácter no sólo de los chicos sino de la juventud en general y por tanto también de las chicas. O más teniendo en cuenta que en los albores del siglo XX, las niñas tenían menos oportunidades de formarse en una sociedad clasista.


{{< img src="https://www.buenacazascout.com/img/historias/guidismo1.jpg" >}}


En un tiempo cuando la falda debía ser hasta los tobillos y las mujeres nunca corrían, la idea de que las niñas participaran en campamentos, caminatas y actividades similares tuvieron diferentes respuestas. Furiosos críticos acusaron al “escultismo femenino” como una “nueva perversa revelación”, un “tonto y pernicioso movimiento”, un “estúpido deporte”.

Sin embargo, las niñas ganaron. En 1910, Baden Powell organizó las Guías Scouts. El término “Guías” lo tomó BP del valeroso Cuerpo de Guías Británicos que conoció en la India, e intentaba dar una idea de romance y aventura mientras indicaba también sus futuras responsabilidades para dirigir a su compañero y educar a sus hijos. Además de esto, éste nombre significaba que eran las que señalaban el camino para otros, las que ayudaban a escalar difíciles alturas.


{{< img src="https://1.bp.blogspot.com/-MKO9DYCYZ0Q/T5ArGVoo0SI/AAAAAAAACHs/YIH2BPHQCD8/s1600/familyship.jpg" >}}


El objetivo principal de su entrenamiento era similar al de los Scouts:Desarrollo de su personalidad y de su salud, y formar un sentido de servicio hacia los demás; además de darles una instrucción práctica para el mantenimiento del hogar y de sus actividades como futuras madres; tarea que requiere de mucha habilidad, coraje, constancia, disciplina y conocimientos.

Baden Powell pidió a su hermana mayor Agnès que se pusiera al frente de la nueva organización. Unos años más tarde su esposa Olave se involucraría también en el Movimiento y, en 1918, fue nombrada Jefe Guía de la Asociación Británica de Guías Scouts. El Guidismo se inició para todas, y con ese fin, se creó una nueva rama para chicas con necesidades educativas especiales llamada Sección de Extensión.



## Nace la Asociación Mundial del Movimiento Guía.



En 1928 las dirigentes adultas del Guidismo también se reunieron y decidieron formar una Asociación que incluyera a todos los países donde hubiera Guías. Esto fue necesario ya que el Guidismo se había hecho tan popular que en algunos lugares la gente usaba los métodos guías con fines equivocados resultando un falso Guidismo. Pero si una compañía estaba debidamente registrada en una Asociación Guía, reconocida por la Asociación Mundial de Guías Scouts (AMGS ó WAGGS en sus siglas en inglés), entonces todos saben que sus miembros son verdaderas Guías que creen en la Promesa la Ley, obedecen las reglas establecidas por la Asociación Mundial y están haciendo el Guidismo en la forma en la que Baden Powell quisiera que fuera.Tan grande fue el entusiasmo por el Guidismo que éste pronto se esparció por todo el mundo y desde los primeros días, incontables millones de mujeres han hecho su promesa Guía.



## Trebol



{{< img src="https://www.buenacazascout.com/img/historias/guidismo3.jpg" >}}


El trébol mundial, símbolo de la Asociación Mundial de Guías Scouts, es un trébol azul (u oro con fondo azul).

Las tres hojas representan las tres partes de la promesa, según fue establecida por el fundador.

La llama representa la llama del amor a la humanidad, que se quema brillante e intensamente en los corazones de todas las Guías.

La vena apuntando hacia arriba es la aguja de la brújula que señala el camino.

Las dos estrellas a cada lado de las hojas del trébol son las estrellas que guían, para siempre recordar la Promesa y los puntos de la Ley Guía.

El círculo exterior representa la asociación a nivel mundial.

El escudo mundial puede ser usado por cualquier miembro de la Asociación Mundial (AMGS o WAGGGS en sus siglas en inglés) que haya hecho la Promesa. Puede ser usado con el uniforme o sin él.
