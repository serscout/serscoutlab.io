---
title: 'Dia del educador Scout'
date: '2019-08-06'
description: 'Por todos y cada uno de nosotros que nos esforzamos por ver crecer a nuestros chicos y lloramos al verlos partir, hoy es nuestro día.'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "noticias"
tags: ["dia", "dirigente", "educador"]
img: 'https://i.postimg.cc/sftxFRKR/educador.jpg'
authors: ["Joaquin Decima"]
---

Cuantos iniciamos con un “son 4 horitas los sábados nomas”, cuantos creímos que era simple ser educador. Por todos y cada uno de nosotros que nos esforzamos por ver crecer a nuestros chicos y lloramos al verlos partir, hoy es nuestro día.




# Historia



{{< img src="https://cardenalferrari.com.ar/wp-content/uploads/2015/08/6Agosto2.jpg" >}}



En 1920 scouts de todo el mundo se congregaron en Londres dando comienzo al primer Jamboree Mundial. La última noche de este Jamboree, un día como hoy 6 de Agosto, Baden Powell fue proclamado Jefe Scout Mundial. Al agradecer el nombramiento dijo:
Si me dieran a elegir entre todos los cargos, elijo el de Guía de Patrulla.



{{< img src="https://cardenalferrari.com.ar/wp-content/uploads/2015/08/6Agosto.jpg" >}}



{{< cita text="Guías de Patrulla, adiestren totalmente a vuestras patrullas… El paso más importante en este sentido es predicar con el propio ejemplo, pues lo que ustedes hagan, será lo que hagan sus scouts." name="B.P. – Escultismo Para Muchachos" >}}
