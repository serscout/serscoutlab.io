---
title: 'Recetario Scout III'
date: '2020-09-03 16:09:00'
description: 'Seguimos con esta seccion de Recetario Scout donde podras ver las mejores recetas para tus campamentos'
type: 'publicaciones'
categories: ["raiders","tropa","rovers"]
color: "universal"
theme: 'cocina'
tags: ["campamento", "supervivencia", "tropa", "unidad", "caminantes", "raiders", "cocina", "vegetarianismo", "vegetariano"]
img: 'https://i.postimg.cc/bvW9X97j/image.png'
authors: ["Norberto Daniel Ceballos"]
---



¿Quién dice que la comida de campamento tiene que ser monótona? ¿o qué siempre tienen que estar presentes los mismos platos? En este blog, vamos a abrir una nueva sección donde puedas tomar estas recetas y sorprender a tus compañeros de patrulla o de comunidad.

Hoy, en **“Recetarios Scout”** vamos a hablar de un nuevo modo de vida que está creciendo tanto entre adultos como entre los jóvenes, el vegetarianismo. Vamos a presentar tres recetas fáciles de cocinar, que pueden ser tanto platos principales como acompañantes de otros alimentos, pero que poseen un sabor increíble que van a dejar a tus compañeros sin palabras a la hora de degustarlos en un campamento. Así que… ¡Manos a la obra!




## Tomates rellenos



¿Los tomates sobre una parrilla? ¡Si!, un sabor diferente al tradicional tomate que se come en ensaladas, con un relleno muy rico que deja a cualquiera con la boca abierta ante tal explosión de sabor.


{{< img src="https://i.postimg.cc/tCd1pNxH/image.png" >}}


* **Tiempo:** 45 minutos (Aprox.).
* **Ingredientes:** Tomates, Ajo, Papel aluminio, Queso (a elección), Aceite, Sal, Pimienta, condimentos.



**Preparación:** Lo que nos va a llevar más tiempo de esta receta es la preparación de la pasta de ajos, así que arrancamos con eso: Para esto agarraremos la cabeza de ajo, sin sacarle la cascara, y le vamos a cortar las puntas (½ centímetro), lo vamos a colocar en el papel aluminio y lo condimentamos con la sal, los condimentos que queramos y, por último, se le pone un chorrito de aceite. A todo esto, se lo envuelve y se lo lleva a las brasas por unos 30 minutos aproximadamente, hasta que tengan una contextura tierna.
Mientras la pasta se está cocinando, procedemos con la preparación de los tomates. Para ello, se agarra el tomate y se le corta una tapa, con una cuchara se le saca el relleno, (Que se puede usar para una salsa en otro momento), y se lo pone a dorar boca abajo o unos 5 minutos o hasta que dore.
Cuando los ajos se pusieron tiernos, se los saca de las brasas y se los coloca en un bowl o un plato, aplastándolos para sacarlos más fácil, y se los aplasta con un tenedor. Por último, se rellena al tomate con el queso y la pasta de ajos preparada y se coloca en la parrilla hasta que se derrita el queso (Se le puede colocar la tapa que se cortó anteriormente para facilitar la cocción interna). Una vez todo listo, ¡se puede disfrutar de un plato exquisito!




## Cebolla detonante



¿Por qué “cebolla detonante” el nombre de esta receta? Porque detona varios sabores a la hora de probarla. Una receta muy fácil de realizar y con ingredientes accesibles, que capaz no llevamos siempre a un campamento, pero es una buena manera de reinventarse.


{{< img src="https://i.postimg.cc/mg5TsP8s/image.png" >}}


* **Tiempo:** 45 minutos (Aprox).
* **Ingredientes:** Cebolla, berenjena, papel aluminio, queso a gusto, manteca o aceite, condimentos, sal.



**Preparación:** Como en la receta anterior, arrancamos por el relleno primero: Para esto colocamos la berenjena sin nada unos 10 minutos a la brasa, la idea es que se queme por afuera pero el relleno se mantenga intacto.
Por otro lado, agarramos la cebolla, le cortamos una tapa y le sacamos la cascara. Con cuidado, se le saca unas capas de relleno y dejamos un molde para que podamos rellenar posteriormente. Una vez que pasaron los 10 minutos, se saca la berenjena, se la corta a la mitad, con una cuchara se retira el relleno y se lo coloca con un bowl o un plato. Teniendo la berenjena, se coloca el queso, la manteca o el aceite, (Según lo que se tenga), y se condimenta con sal y alguna otra especia a gusto. Se mezcla todo esto con un tenedor y se rellena la cebolla que anteriormente habíamos ahuecado. Una vez hecho todo esto, se envuelve a la cebolla en papel aluminio y se lo coloca a la parrilla o a las brasas, según como se sientan más cómodos, por unos 30 minutos aproximadamente para asegurarse la cocción de la cebolla.   




## Tortilla de verduras



La tortilla es una receta que puede llevar cualquier combinación de verduras. Es rica, fácil y rápida de hacer, con materiales sencillos, y es especial para los días de verano, ya que se puede comer tanto fría como caliente.


{{< img src="https://i.postimg.cc/Fz49BwGk/image.png" >}}


* **Tiempo:** 30 minutos.
* **Ingredientes:** Papas, berenjenas, cebolla, morrón, aceite, huevos, condimentos.



**Preparación:** Primero se agarran las verduras para rellenar y hacer la tortilla, se lavan bien bajo agua y se cortan en láminas finas. Mientras tanto, se bate un par de huevos en algún bowl, según la cantidad de personas que coman y la cantidad de verduras que le pongas a la tortilla. Con más huevos quedará mucho más esponjosa y la verdura no quedará apelmazada. A estos huevos batidos se les puede agregar los condimentos y la sal que se desea para que quede con sabor.
Por otro lado, se corta y se pica la cebolla y el morrón, para posteriormente saltearlos en una sartén con aceite. Una vez que esas verduras están transparentes, se coloca las verduras cortadas en laminas unos minutos para que agarren sabor.
Saca la verdura salteada y mézclala con el huevo batido, se trata de remover y mezcla bien todo. Se añade la mezcla a fuego muy lento en tu sartén, se cocina bien de todos lados y se sirve en un plato para cortar en porciones iguales para todos.




¡Es momento de poner tus manos a la obra!, estas recetas pueden servir de guía, pero a todo lo que prepares ponele tu toque especial y hacelas propias. Si queres compartir alguna receta, déjala en los comentarios y entre todos construyamos un estilo culinario de los Scout. ¡Siempre Listo!
