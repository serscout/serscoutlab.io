---
title: 'El poder de la narración'
date: '2020-09-23 10:30:00'
description: 'La narración es una herramienta muy importante para todos los scouts y queremos compartir nuesta idea con vos'
type: 'publicaciones'
categories: ["raiders","tropa","rovers","manada"]
color: "universal"
theme: 'historias'
tags: ["campamento","narracion","tropa","unidad","caminantes","raiders","manada","narración","poder","beedle","bardo","cuentos","historias"]
img: 'https://i.postimg.cc/ZqnpwxvT/image.png'
authors: ["Joaquin Decima"]
---

Nosotros como scouts muchas veces recurrimos al *poder de la narración* sabemos que las historias que envuelven los juegos le dan magia y motivan a los jóvenes a seguir adelante con entusiasmo.




## Lo importante de contar…


{{< img src="https://i.postimg.cc/ZqnpwxvT/image.png" >}}


Muchas personas pueden contar historias, y muchas veces vemos como le ponen sus propios tintes a historias que por ahí ya habíamos escuchado… Sin embargo la narración es una *habilidad* que debemos ir puliendo constantemente y debemos ir transmitiendo a nuestros jóvenes.




## Para algo más…


{{< img src="https://sites.google.com/site/398cuentosclasificados/_/rsrc/1315709601780/narracion-literaria/reading-story-book%5B1%5D.jpg" >}}


Narrar una historia o un cuento no es solo magia, no es solo dar el contexto para nuestras actividades, es hacer volar la imaginación, es poder llegar a los sentimientos de los jóvenes y los niños, plantear una refleccion que para algunos jóvenes pueden ser la luz en el camino obscuro.




## Diferencia entre narradores



En esta oportunidad les queremos dejar una actividad que plantea 3 tipos de narradores diferentes y forzados. Vemos como la forma de contar una historia puede darle tintes realmente diferentes


{{< youtube code="tQkZLyNLdc4" >}}


Para esta oportunidad usamos *Los cuentos de Beedle el bardo* de *Harry Potter* para tener un tinte magico.




## Tu experiencia



Queremos escuchar más tus opiniones, tus historias, déjanos la historia que fue más significativa para vos. Si te animas mándanos un video contandola y compartamos con nuestros hermanos todas las hermosas historias que nos marcaron de alguna u otra forma.


