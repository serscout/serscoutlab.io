---
title: 'Mano Izquierda'
date: '2020-05-19'
description: 'Una pequeña historia que explica porque los socuts saludamos con la mano izquierda'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "historias"
tags: ["historias", "izquierda", "mano", "saludo"]
img: 'https://scontent.faep9-1.fna.fbcdn.net/v/t1.0-9/96125294_689581338461611_290682901261975552_n.png?_nc_cat=104&_nc_sid=0be424&_nc_ohc=oAb0ixeT-4IAX9NctIc&_nc_ht=scontent.faep9-1.fna&oh=bd9ffc99652bc18fda5866aa3f33633a&oe=5EEA3144'
authors: ["Joaquin Decima"]
---

Cuando BP capturó al jefe “Premphe” de la Tribu de los Ashanti decidió que para impedir un derramamiento inútil de sangre era más caballeresco y prudente firmar un tratado de paz.

Y así se hizo, finalizado el acto protocolario Baden Powell extendió su mano derecha para estrechar la mano de “Premphe” como muestra de respeto y amistad, pero el jefe Ashanti respondió con un acto hasta entonces inusual, soltó el escudo de guerrero que portaba en la mano izquierda y la extendió a aquel militar Británico insistiendo en hacerlo.

Baden Powell, algo confundido para evitar algún malentendido tomo la mano del jefe y la estrecho.

Como Baden Powell nunca había hecho nada parecido en toda su vida pregunto al gran jefe “Premphe” que por que con la mano izquierda y no con la derecha a lo que el jefe respondió:

“No, en mi país el más bravo entre los bravos da la mano izquierda. Nosotros para demostrar y reconocer que alguien es nuestro amigo nos privamos de nuestra coraza que es nuestro escudo en señal de confianza y buena fe"



Fuente: https://www.facebook.com/castorcojuelo54/posts/1714169422074644
