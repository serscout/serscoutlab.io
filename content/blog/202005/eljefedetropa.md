---
title: 'El jefe de tropa y el campamento'
date: '2020-05-07'
description: 'De todas las escuelas, el campamento es, sin duda ninguna, la mejor para enseñar a los chicos las cualidades del carácter que se desea inculcarles.'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "historias"
tags: ["historias", "tropa", "jefe", "campamento"]
img: 'https://i.postimg.cc/bYn3d8ND/images.jpg'
authors: ["Joaquin Decima"]
---

"De todas las escuelas, el campamento es, sin duda ninguna, la mejor para enseñar a los chicos las cualidades del carácter que se desea inculcarles.

Allí el ambiente es saludable; los muchachos se vuelven ambiciosos y perspicaces; les rodea todo un mundo interesante, y el Jefe de Tropa tiene a ésta constantemente, día y noche, bajo su dirección.

El Jefe de Tropa tiene en el campamento, según puede notarse, la mayor oportunidad de observar y conocer las características individuales de cada scout, para después encauzarlas en la dirección conveniente a su desarrollo; y los muchachos a su vez van adquiriendo las cualidades del carácter inherente a la vida campestre, en la cual el comprensivo Jefe de Tropa puede inculcarles, con jovial y bondadosa dirección, disciplina, habilidad, ingenio, confianza, destreza, conocimiento de los bosques, manejo de canoas, espíritu colectivo, los secretos de la naturaleza, etc.

Una semana de esta clase de vida equivale a seis meses de enseñanza teórica en las aulas por excelente que ésta sea.

Por lo expuesto anteriormente se aconseja que el Jefe de Tropa poco experto en ese ramo estudie el arte de acampar en sus distintos aspectos."

Extraído de libro "Guía para el Jefe de tropa", Lord Baden-Powell of Gilwell, Jefe Scout Mundial.

Nota: el campamento Scout es de 8 a 10 días.

Fuente: https://www.facebook.com/profile.php?id=100000299768817
