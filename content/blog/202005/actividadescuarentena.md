---
title: 'Actividades para la cuarentena'
date: '2020-05-22'
description: 'No te pierdas la increible propuesta de juegos que tiene Dani para esta cuarentena'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "noticias"
tags: ["noticias", "actividades", "covid19", "cuarentena"]
img: 'https://www.scout.es/wp-content/uploads/2020/04/adicciones.jpg'
authors: ["Norberto Daniel Ceballos"]
---

Despertarte a la mañana y sentir el pasto mojado por la escarcha, preparar el fuego matutino para el desayuno son cosas que cualquier scout podría estar extrañando de un campamento a estas alturas. A todos, tanto beneficiarios como educadores, no han enseñado que gran parte de nuestras vivencias como scout se realiza haciendo actividades en la naturaleza, pero en estos momentos donde nuestra buena acción es quedarnos en nuestras casas… ¿Se puede hacer actividades scout vía internet?

La respuesta es: ¡Si!, sabemos que a veces no tendrá el mismo impacto que estar frente a tus beneficiarios o frente de tus compañeros de equipo, pero depende de la voluntad y los recursos que pongamos a disposición para hacerlas, serán muy fructíferas. En estos pocos párrafos te vamos a dar algunas herramientas para que, (si quieres), las puedas aplicar en tus reuniones virtuales:




## Juegos


{{< img src="https://http2.mlstatic.com/juegos-clasicos-block-tutti-frutti-ahorcado-batalla-naval-D_NQ_NP_775496-MLA32099319778_092019-F.jpg" >}}


Los juegos siempre son un buen momento para compartir entre todos y generar risas en una sana competencia entre compañeros; aprendemos a socializar y nos ayuda a desarrollar un sentido de "pertenencia al grupo", pero: ¿cómo se aplican los juegos? Bueno, desde una opinión personal, es buen momento para volver a los juegos clásicos (como una sopa de letras, un tutti frutti, ahorcado, entre otros), pero siempre dando una vuelta de rosca y volviéndolos atractivos para todos como, por ejemplo, establecer un tiempo y competencia de equipos para una sopa de letras buscando palabras claves de elementos para un campamento. Para tener en consideración, hay muchas páginas de internet que nos ayudan a crear varios de estos juegos y compartirlos con los demás. Como ya nombramos en otra publicación (Si no la leyeron, pueden buscarla como “{{< textlink url="/posts/2020/05/aplicaciones-para-la-cuarentena/" text="Aplicaciones para la cuarentena" >}}”) el modo Pizarra de Zoom nos va a permitir dibujar o realizar muchas de estas cosas que nos serian útiles para los juegos y, además, permite que todos puedan escribir en esa misma pantalla.       




## Elementos que hacen a la rama



Se entiende que hay elementos de las ramas que en estas circunstancias es muy difícil realizarlas (Como un Raid en los Raiders o Caminantes, un campamento en soledad de los Rover, una cacería con la Manada o  un campamento de la Tropa scout), pero estos momentos son también para focalizarnos en otros elementos que son propios de nuestra rama y que se pueden realizar en esta época de aislamiento social: Podemos juntar a nuestra manada y hacer un relato de “El libro de las tierras Vírgenes” o fortalecer el conocimiento de la Ley de la manada, las reuniones por patrulla o hablar y explicar los roles dentro del equipo en la Tropa Scout, y encarar los objetivos personales de las ramas mayores, ya sea la hoja de marcha en los caminantes o el PPA en los Rovers. Son momentos para poder destacar la creatividad de muchos de nuestros beneficiarios o compañeros, con los objetos que tenemos en nuestras casas, y ponerlos al servicio ya sea para hacer un taller, explicar algún concepto o para una especialidad. Obviamente también es un buen momento para encarar alguna que otra actividad reflexiva (si se quiere hasta con temática de la confesión religiosa que te parezca), pero… ¡ojo! Abajo te vamos a dar algunas recomendaciones.




## Talleres


{{< img src="https://wwwhatsnew.com/wp-content/uploads/2019/05/talleres-gratuitos-730x487.jpg" >}}


Nunca está de más la realización de talleres para aprender cosas nuevas, y más sabiendo de la cantidad de tutoriales que existen en internet para poder hacer cualquier cosa que nos imaginemos, pero… ¿Cómo los podemos realizar?

En primer lugar, elijamos alguna de las aplicaciones de videollamada con la cual nos sintamos mas cómodos para poder hacerlas (Zoom, Whats app, Skype, etc.) y establezcamos sobre que puede ser el taller: Enseñar nudos, Cabuyería artística, primeros auxilios, armar algún elemento que nos sirva para irnos de campamento (Cocinas, faroles caseros, entre otras cosas).  Definamos la cantidad de beneficiarios o compañeros queremos tener en el mismo taller, recomendamos una cantidad máxima de 8 personas para que pueda ser más educativa la experiencia, y en caso de tener una rama con una mayor cantidad de chicos, repartir de forma equitativas en talleres simultáneos si disponemos de las personas para realizarlo. Podemos nosotros ser los que guían este taller o utilizar algún video que encontremos de internet para que este sea el que muestre los pasos a seguir y, como ultima recomendación, realicemos talleres con materiales que todos podamos tener en nuestro hogar o conseguir en los comercios de cercanía para que todos puedan hacerlos.
Algún ejemplo:


{{< youtube code="mqOjvqnwRKU" >}}  



## Consideraciones pedagógicas



A veces es raro pensar en esto, pero es muy útil a la hora de aplicarlo en lo que pensemos como actividades. Si bien los chic@s y jóvenes de hoy en día son mas apegados a la tecnología, siempre se tiene que ver algunos conceptos básicos: La atención es una capacidad mental fundamental para la supervivencia al permitirnos atender a los diversos estímulos ambientales. Varios estudios en neurociencia indican que se calcula que por cada año de vida de un niño tiene entre 3 y 5 minutos de concentración. Por ende, un niño de 7 años, por ejemplo, tiene un tiempo de concentración de entre 20 y 35 minutos. Para un adolescente el tiempo máximo de concentración es de, en promedio, de 45 minutos. En general, se recomienda que se descanse 10 min por cada 45 min de actividades (Si trabajamos sobre actividades específicas como una reflexión o conceptos de la rama importantes, ese descanso de 10 minutos se puede aplicar algún juego que distraiga). Otra cosa para saber es que, aunque la capacidad de mantener la atención varía con respecto a la edad y el interés del beneficiario, lo cierto es que es más que probable que los jóvenes se desconcentren si las actividades online son demasiado largas. El contenido debe estar concentrado y ser de calidad para mantener su interés el máximo tiempo posible.   

Como ya dijimos, son recomendaciones que podemos dar desde nuestro lugar, usa las que creas que son buenas, recomendémonos entre todos actividades e ideas que pueden dejar escritas en nuestros comentarios para generar un gran contenido de actividades scout scout entre todos, y siempre impongámosles nuestra impronta para que salga lo mejor. Saludos y ¡Siempre Listos!  
