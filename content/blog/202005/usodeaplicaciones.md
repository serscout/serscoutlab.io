---
title: 'Aplicaciones para la cuarentena'
date: '2020-05-22'
description: 'Hoy Dani un gran amigo nos recomienda apps para mantener las actividades esta cuarentena'
type: 'publicaciones'
categories: ["raiders","tropa","manada","rovers"]
color: "universal"
theme: "noticias"
tags: ["noticias", "cuarentena", "covid19", "aplicaciones"]
img: 'https://fececo.org.ar/wp-content/uploads/2020/04/1366_2000.jpg'
authors: ["Norberto Daniel Ceballos"]
---

¡Buen día! En esta época de aislamiento social preventivo y obligatorio, el no estar en nuestro patio de grupo nos puede complicar a la hora de desarrollar actividades sábado a sábado con nuestros chicos y se podría perder ese acompañamiento diario que ofrecemos. Para que esto no ocurra, traemos un par de herramientas online que se pueden utilizar para facilitar un poco el desarrollo de las ideas que podamos llegar a tener, y agregaremos alguna **opinión personal** sobre su utilización. Vayamos desglosando lo que el mundo de la internet nos puede ofrecer:




## Google y sus herramientas



Es una de las empresas más conocidas a nivel mundial y ofrece muchas opciones de aplicaciones totalmente gratuitas que son útiles para ciertos aspectos, como, por ejemplo: **Google Calendar** que es una agenda y calendario electrónico y permite sincronizarlo con los contactos de Gmail de manera que podamos calendarizar ciertas actividades que queremos realizar o que tengamos planificadas, tanto para los chicos como para los educadores. **Google Drive** sabemos que nos permite almacenar archivo en la “nube”, pero también se puede utilizar para compartir lecturas o videos que nos parezcan interesantes armando solo una carpeta específica para la rama que estemos trabajando o nuestros compañeros de comunidad; sus hojas de calculo se pueden utilizar para realizar juegos como por ejemplo: “Ta-Te-Ti virtual” donde todos puedan participar y divertirse; las presentaciones de Google se pueden utilizar para mostrar de forma interactiva algún tema principal para la rama, (Relato de un cuento del libro de la selva, trabajar los roles del equipo en la tropa, la hoja de marcha en los caminantes o la presentación del plan personal de acción de los Rovers), y los formularios de Google, (que crearlos son fáciles), se pueden utilizar tranquilamente como un método evaluativo de cualquier actividad. Los usos son muchos, solo tenemos que enfocarlos a lo que necesitamos.


{{< img src="https://canalc.com.ar/wp-content/uploads/2020/05/Google.jpg" >}}


## Zoom u otras plataformas de Videollamada



Sin duda alguna, en este punto es donde se ha dado un boom en la utilización. **Zoom** es un servicio de videoconferencia basado en la nube que puede usar para reunirse virtualmente con otros, ya sea por video o solo audio o ambos, todo mientras realiza chats en vivo, y le permite grabar esas sesiones para verlas más tarde, además, el modo para compartir pantalla (“Share Screen”) es muy útil tanto como para poder compartir imágenes o videos de internet, (aunque a veces suele tildarse, según la velocidad de internet que posea), en su modo pizarra para realizar juegos, escribir textos u algún otro motivo que se nos pueda ocurrir. Por último, cabe recalcar que tiene una opción que es la de sala de reuniones para grupos más pequeños que nos permite dividirnos por equipos y no escuchar a nuestros compañeros. Algunas de las consideraciones que se pueden tener es el tema de la seguridad, por eso se recomienda que todas las reuniones que planifiques tengan contraseña de seguridad.


{{< img src="https://fueracodigos.com/wp-content/uploads/2018/03/zoom-webinars-videoconferencias-tutorial.jpg">}}


Otras aplicaciones que pueden usarse son Whats app, (Aunque solo para videollamada y permite un numero limitado de 8 contactos y todos tienen que tener actualizada la aplicación, recomendable para reuniones de equipo), y Skype también es una buena alternativa para generar reuniones de hasta 50 personas (A veces puede tener mala resolución de imagen o de sonido) o Jitsi. ¡Todo es cuestión de preferencias y habilidad a la hora de manejarlo!




## Plataformas para la visualización de películas o series



Una película nunca viene mal. Si se quiere compartir alguna película o un capitulo de una serie entre compañeros de rama o con un fin educativo, existen varias aplicaciones que se pueden utilizar que son útiles. Netflix Party ofrece un buen servicio para compartir, a tiempo real, cualquier contenido de la plataforma, pero posee la desventaja de que todos tienen que poseer una cuenta. Otra de las que aparece para compartir video es Kast, tanto como para celular o computadora (recomendable computadora) y se puede trabajar con un salón virtual donde se pueden reunir con amigos o beneficiarios para hablar y compartir una película, un programa de televisión o incluso podés ver partidas de videojuegos.


{{< img src="https://images.mediotiempo.com/bONatjw5FgSlwsfD1cWaWCCcCy8=/958x596/uploads/media/2020/03/15/la-solucion-para-no-sentirse.jpg" >}}


## Kahoot!



Es una Aplicación que originalmente se pensó para el ámbito educacional, pero se puede adaptar para lo que es el ámbito scout. Es una herramienta muy útil y la forma más común es mediante preguntas tipo test, aunque también hay espacio para la discusión y debate.
Encontrar un juego o aplicación que se adapte a nuestras necesidades es complicado, y por eso una de las principales ventajas de Kahoot! es que cualquier persona puede crear el contenido para un juego. Lo único que se necesita es registrarse, (que es gratuito para un pack básico), y tener paciencia para preparar las preguntas y respuestas. Lo maravilloso es que se puede generar una competencia entre equipos o entre beneficiarios por preguntas sobre el scoultismo y generar un clima de diversión.  


{{< img src="https://www.educaciontrespuntocero.com/wp-content/uploads/2017/08/kahoot-1.jpg" >}}


Estas son solo de algunas de las aplicaciones que presentamos, (existen muchísimas más), para que puedan realizar actividades variadas y entretenidas con nuestros compañeros de rama o para usar con nuestros beneficiarios. Si vos usas otra y te gustaría compartirla, deja un comentario y entre todos vamos construyendo una red de contención para estos momentos difíciles.



Saludos y ¡Siempre Listos!
