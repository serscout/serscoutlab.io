---
title: 'Como Hacer de Espias'
date: '2020-07-01 12:28:00.00'
description: 'Un hermoso libro de la saga Como Hacer para comenzar a planificar reencuentros y mantener activos a los chicos'
type: 'archivos'
categories: ["tropa","manada"]
color: "universal"
theme: 'supervivencia'
tags: ["como", "hacer", "espias", "creatividad", "biblioteca"]
img: 'https://http2.mlstatic.com/como-hacer-de-espias-codigos-secretos-trucos-ed-plesa-D_NQ_NP_637911-MLA41080779634_032020-F.jpg'
authors: ["Joaquin Decima"]
download: "http://www.mediafire.com/file/ar80upciradcvfn/Como-Hacer-de-Espias-Falcon-Travis.pdf/file"
---

Ya anteriormente hablamos de {{< textlink url="/documents/2020/05/como-hacer-de-detective/" text="otro libro" >}} de esta saga de “Como Hacer” dónde dimos ideas de cómo implementar estos libros por ejemplo en un evento zonal, en esta oportunidad vamos a traer otro libro que puede cautivar a los jóvenes para poder dar temática a otro evento o porque no combinarlo con el anterior!

{{< img src="https://http2.mlstatic.com/como-hacer-de-espias-codigos-secretos-trucos-ed-plesa-D_NQ_NP_637911-MLA41080779634_032020-F.jpg" >}}

Este libro trata de las formas de ocultar secretos y de comportarse disimuladamente. En él, podrás aprender a camuflar mensajes y mapas, a transmitirlos solapadamente y a concertar citas que pasen inadvertidas para el resto de la gente. También tienes en él, diversos tipos de claves para escribir mensajes y varios métodos secretos para comunicarse por medio señales. Tematica recurrente en el escultismo.

Desde la primera página, vas a conocer al Espía del Sombrero Negro. Atención a los trucos que emplea y a la manera de proceder, pues así es como los espías operan en la realidad. En las páginas de este libro, encontrarás muchos mensajes en clave. Prueba a descifrarlos por ti mismo...
