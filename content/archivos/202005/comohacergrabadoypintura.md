---
title: 'Como Hacer Grabados y Pinturas'
date: '2020-05-21'
description: 'Un hermoso libro de la saga Como Hacer que nos ayuda a mantener las actividades a distancia y/o planear un reencuentro.'
type: 'archivos'
categories: ["tropa","raiders","manada"]
color: "universal"
theme: "biblioteca"
tags: ["como", "hacer", "grabado", "pintura", "biblioteca"]
img: 'https://image.slidesharecdn.com/comohacergrabadosypinturas-heatheramery-170310143109/95/como-hacer-grabados-y-pinturas-heather-amery-1-638.jpg'
authors: ["Joaquin Decima"]
download: "http://www.mediafire.com/file/gl20k8svxmxyrj1/Como-Hacer-Grabados-y-Pinturas-Heather-Amery.pdf/file"
---

En esta ocasión traemos uno de los libros de la saga de “Como Hacer” se trata de «Como Hacer Grabados y Pinturas» un manual que desata nuestra creatividad y nos enseña diferentes formas de plasmarla.

Estos manuales tienen mucho contenido interesante y desafiante para los chicos facil de realizar que no requiere experiencia previa ni materiales difíciles de conseguir.

{{< img src="https://image.slidesharecdn.com/comohacergrabadosypinturas-heatheramery-170310143109/95/como-hacer-grabados-y-pinturas-heather-amery-1-638.jpg">}}

Cuando repasaba vagamente las hojas del libro se me ocurrieron muchas ideas de como utilizar el contenido de este libro y comparto algunas con ustedes, recuerden que siempre es bueno indagar y experimentar más:

* Decorar el libro de oro con la huella de los miembros el dia de su ingreso
* Crear un libro donde se estampen todas las hojas recolectadas por campamentos con lugar y año (esto puede incluir sistema de puntos)
* Crear sello identificatorio de seisena o patrulla

Y seguramente existan mil usos mas con esto que incluso podamos diseñar desde la cuarentena para mantener el nivel de actividad con los chicos
