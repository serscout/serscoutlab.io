---
title: 'Como Hacer de Detective'
date: '2020-05-20'
description: 'Un hermoso libro de la saga Como Hacer que nos ayuda a mantener las actividades a distancia y/o planear un reencuentro.'
type: 'archivos'
categories: ["tropa","raiders","manada"]
color: "universal"
theme: "biblioteca"
tags: ["como", "hacer", "detective", "biblioteca"]
img: 'https://imgv2-1-f.scribdassets.com/img/document/235369675/original/c0bf5d3ed8/1587847873?v=1'
authors: ["Joaquin Decima"]
download: "http://www.mediafire.com/file/y2pzi7do96nc7zk/Como-Hacer-de-Detective-Falcon-Travis.pdf/file"
---

En esta ocasión traemos uno de los libros de la saga de “Como Hacer” se trata de «Cómo hacer de detective» un manual para futuros investigadores que relataba los pasos a seguir para desenmascarar cualquier crimen. En él, se nos narraba la historia del detective Camaleón, su ayudante Gonza-lín y un misterioso ratero conocido como «El Escurridizo».
A medida que la historia discurre, en un cómic ameno y bien dibujado, el libro nos iba proveyendo de información sobre los métodos de investigación empleados y como podíamos reproducirlos en casa: cómo interrogar a un testigo, hacer un retrato robot, redactar una ficha criminal, o buscar huellas dactilares en el escenario del crimen…

{{< img src="https://imgv2-1-f.scribdassets.com/img/document/235369675/original/c0bf5d3ed8/1587847873?v=1" >}}

Este hermoso libro no solo nos inspira, tiene muchas manualidades que pueden ser utilizadas para un reunion tematica entre grupos y demas, creando una historia similar o simplemente replicandola, conllevando a los jóvenes a crear sus propias herramientas tal cual las muestra el libro…


También es muy útil para enviarselo a los jóvenes en esta cuarentena para distraerse un poco.
