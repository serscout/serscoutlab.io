---
title: 'Como Hacer Juegos de Accion'
date: '2020-05-26'
description: 'Un hermoso libro de la saga Como Hacer que nos ayuda a mantener las actividades a distancia y/o planear un reencuentro.'
type: 'archivos'
categories: ["tropa","raiders","manada"]
color: "universal"
theme: "biblioteca"
tags: ["como", "hacer", "juegos", "accion", "biblioteca"]
img: 'https://image.slidesharecdn.com/comohacerjuegosdeaccion-annecivardi-170310143151/95/como-hacer-juegos-de-accion-anne-civardi-1-638.jpg?cb=1489157781'
authors: ["Joaquin Decima"]
download: "http://www.mediafire.com/file/g3y1u4moe55us5e/Como-Hacer-Juegos-de-Accion-Anne-Civardi.pdf/file"
---

En esta ocasión traemos un libro de la saga “Como Hacer” el cual viene con unas excelentes ideas de juegos de mesa, entre otras cosas, que nos serán muy útiles para esta cuarentena. Y en general para poner en acción la creatividad de los niños.

{{< img src="https://image.slidesharecdn.com/comohacerjuegosdeaccion-annecivardi-170310143151/95/como-hacer-juegos-de-accion-anne-civardi-1-638.jpg?cb=1489157781" >}}

En este año de Cuarentena y reuniones cortadas podemos, con estas creativas ideas podemos mejorar la interacción y dales un poco de actividad.
