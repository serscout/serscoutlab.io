---
title: 'Escultismo para muchachos'
date: '2019-08-21'
description: 'Este libro ya habla por sí solo por lo cual está de más cualquier presentación. Todos deberiamos tenerlo a mano siempre.'
type: 'archivos'
categories: ["tropa","raiders"]
color: "universal"
theme: "biblioteca"
tags: ["escultismo", "muchachos", "bp", "biblioteca"]
img: 'https://i.postimg.cc/zvW9pjGP/9788432920615.jpg'
authors: ["Joaquin Decima"]
download: "https://www.mediafire.com/file/5nxbicbsvu6bhd6/Escultismo_para_Muchachos.pdf/file"
---

Para todos los que ya estamos en el palo seguramente lo escuchamos nombrar en algún momento. Este libro es una de las bases más fuerte del escultismo y como tal todos dentro del movimiento deberíamos leerlo, indagarlo e incluso tenerlo a mano, si es posible en papel.

{{< img src="https://i.postimg.cc/tRxQXXNt/epm-indice.png" >}}
