---
title: 'Manual de Cabulleria'
date: '2019-08-14'
description: 'Hoy traemos un manual de cabulleria o “handbook” de nudos que cuenta con la información mínima que debemos transferir a nuestros jóvenes.'
type: 'archivos'
categories: ["tropa","raiders","manada","rovers"]
color: "universal"
theme: "campamento"
tags: ["cabulleria", "nudos", "campamento", "supervivencia", "sogas"]
img: 'https://i.postimg.cc/sD85c705/tapa.png'
authors: ["Joaquin Decima"]
download: "https://www.mediafire.com/file/tedis4muavek1dh/Manual_Cabulleria.pdf/file"
---


El siguiente manual me lo cruce en la página de SAAC cuando buscaba toda la información que requieren los jóvenes de cabulleria. Me sorprendió al ver que es muy completo y contiene mucha info para transferir. El siguiente manual me lo cruce en la página de SAAC cuando buscaba toda la información que requieren los jóvenes de cabulleria. Me sorprendió al ver que es muy completo y contiene mucha info para transferir.

{{< img src="https://i.postimg.cc/xTqKMtg0/indice.png" >}}

Las explicaciones se encuentran a nivel de los chicos y tenemos contenido para enseñarles a desenvolverse. Desde mi punto de vista está Bueno articularlo con algunas actividades como Carrera de nudos. O también es útil como un manual para una patrulla o equipo.
