---
title: 'Como Hacer Jueguetes que Funcionan'
date: '2020-06-10'
description: 'Un hermoso libro de la saga Como Hacer que nos ayuda a mantener las actividades a distancia y/o planear un reencuentro.'
type: 'archivos'
categories: ["tropa","raiders"]
color: "universal"
theme: "biblioteca"
tags: ["como", "hacer", "juguetes", "biblioteca"]
img: 'https://4.bp.blogspot.com/-FRYkiX0pM2I/WKTYjBqqoAI/AAAAAAAAJ8A/7pUzOzhwbmcXJODiAZltrxdQbz4bJtPQQCLcB/s1600/C%25C3%25B3mo%2BHacer%2BJuguetes%2Bque%2BFuncionan.png'
authors: ["Joaquin Decima"]
download: "http://www.mediafire.com/file/eiwxo3pn3gbk85f/Como-Hacer-Juguetes-Que-Funcionan-Heather-Amery.pdf/file"
---

Este libro es impecable, nos motiva a desarrollar un poco mas el area de la creatividad. Este tipo libro podemos usarlo para diferentes servicios ademas de mejorar las habilidades manuales.

Este libro trata de varias formas de hacer juguetes, maquinas, modelos y juegos en casa. Fabrícalos tú mismo, usando los materiales que tengas a tu alcance u otros que fácilmente puedas conseguir en papelerías, etc.
Para hacer casi todos los juguetes de este libro, lo que vas a necesitar será:

* Papel
* Cartón
* Cartulina
* Botellas
* Envases de plástico
* Tubos de cartón y rollos
* Pajitas de beber
* Cajas
* Plastilina.

Las medidas usadas en cada juguete son una guía. Puedes hacer las cosas del tamaño que más te guste. Según vayas haciendo los juguetes, los podrías ir recubriendo de papeles de colores o bien los puedes pintar una vez que los tengas terminados.


{{< img src="https://4.bp.blogspot.com/-FRYkiX0pM2I/WKTYjBqqoAI/AAAAAAAAJ8A/7pUzOzhwbmcXJODiAZltrxdQbz4bJtPQQCLcB/s1600/C%25C3%25B3mo%2BHacer%2BJuguetes%2Bque%2BFuncionan.png" >}}

## Contenido

* Insecto Lunar que se Arrastra
* Rulotes y Tentetiesos
* El Perrito Torpón
* Juguetes Ruidosos
* El Tractor Titan
* Tormenta de Nieve
* El Tiovivo Veloz
* Delta, el Avión a Reaccion
* Dandy, el Dragón Saltarin
* Haciendo un Periscopio
* Haciendo un Caleidoscopio
* Muchas Gracias, la Caja Registradora
* Saturno, el Cohete Espacial en dos Fases
* Un Equilibrista
* El Ciclista Truquista
* Los Bomberos
* Haciendo un Cine
* Dibujos Animados
* Una Hélice de Gran Potencia
* Un Globo Super-Rapido
* Grúa con Gancho Acoplable
* Haciendo un Telar
* Un Barco a Vela
* El Settor Muecas
* El Bilido. Formula XF
* Un Reloj de Agua
* Galería de Tiro al Blanco
* Carreras de Autos Grand Prix
