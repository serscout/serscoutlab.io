---
title: 'Cuaderno de Construcciones (ASDE)'
date: '2021-04-22'
description: 'Os presentamos una nueva publicación de la serie Técnicas Scouts, en esta ocasión
desarrollamos la línea de construcciones scouts.'
type: 'archivos'
categories: ["tropa","raiders","manada","rovers"]
color: "universal"
theme: "campamento"
tags: ["cabulleria", "nudos", "campamento", "supervivencia", "sogas","construcciones","asde","españa"]
img: 'https://i.postimg.cc/tCw1jcDc/image.png'
authors: ["Joaquin Decima"]
download: "https://www.mediafire.com/file/c1sy3ibny1p5e6d/Cuaderno_Construcciones_A.pdf/file"
---

Os presentamos una nueva publicación de la serie Técnicas Scouts, en esta ocasión
desarrollamos la línea de construcciones scouts.

{{< img src="https://i.postimg.cc/ncDFvhYd/image.png">}}

### EDITA:


ASDE Scouts de España


### COLABORAN:


**Coordinador:** Javier Atiénzar Martínez. Equipo Educación Ambiental y para el Consumo responsable. Scouts de España


* Sheila Vilches Crespo . Scouts de Andalucía
* René Díaz González. Exploradores del Principado de Asturias
* María Rojas Estévez. Scouts de Canarias
* Jesús Vega Rossell. Scouts de Extremadura
* Raúl Fuentes Solís. Scouts de Extremadura
* Fernando González Muiño. Scouts de Galicia
* Héctor Ruíz Ortega. Scouts de La Rioja
* Tomás de Garay Herrera. Exploradores de Madrid
* Iris Cervantes Moreno. Exploradores de Madrid
* Marta Mora Navarro. Scouts de Melilla
* Alfonso Sevilla Prieto. Scouts Valencians


### AGRADECIMIENTOS:


* Grupo Scout Mare Nostrum 333. Scouts de Andalucía.
* Grupo Scout Keltikhé 635. Exploradores de Asturias.
* Grupo Scout Acanac 486. Scouts-Exploradores de Canarias.
* Grupo Scout San Jorge 103. Exploradores de Castilla y León.
* Grupo Scout Arlanzón 688. Exploradores de Castilla y León.
* Grupo Scout Antorcha 687. Exploradores de Castilla y León.
* Grupo Scout Manitoba 332. Exploradores de Castilla y León.
* Grupo Scout Laguna 589. Exploradores de Castilla y León.
* Grupo Scout Ciudad del Sol 108. Exploradores de Murcia.


Un reconocimiento y agradecimiento especial a todos los grupos scouts que habéis participado compartiendo vuestras fotos de construcciones en redes sociales.

### REVISIÓN:


Servicio de Educación Scout bajo la coordinación de David Navalón Mateos y Equipo técnico de ASDE.


## TERMINOLOGÍA Y GLOSARIO DE TÉRMINOS


En España tenemos gran variedad de términos y muchas palabras para decir una misma cosa. Por otra parte están las palabras que nos inventamos, “el coso” “el éste que está en el ese”…

Haciendo esta publicación hemos encontrado la misma problemática, algunas cosas que se nombran así “de toda la vida” en otro lado se nombran de otro modo “de toda la vida” y a la hora de realizar las construcciones que se muestran a continuación, ha pasado un poco lo mismo… Así que abajo se mencionan los términos que quizás puedan resultar más confusos o conflictivos, ¡ánimo!


* Cualquier elemento circular, que hubiese formado parte de un árbol, de madera que se use para hacer construcciones (madero, palo, tronco) debería denominarse… TRONCO.
* Cordel trenzado, formado por fibras de aspecto vegetal, empleado para hacer las construcciones… CUERDA PITA.
* Lona o trozo de tela que permite cubrir algo… RAFIA.
* Elemento de madera, de pequeño tamaño, proveniente de un árbol que no tienen las dimensiones suficientes para considerarlo tronco… RAMA.
* Alcotana, picoleta, Zapa-Pico, picomartillo… PICOLETA.
