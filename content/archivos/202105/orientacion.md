---
title: 'Manual Basico de orientacion'
date: '2021-04-22'
description: 'Un muy interezante manual con grandes tecnicas de orientacion util para todas las ramas'
type: 'archivos'
categories: ["tropa","raiders","manada","rovers"]
color: "universal"
theme: "campamento"
tags: ["orientacion", "manual", "campamento", "supervivencia", "brujula", "construcciones","asde","españa"]
img: 'https://i.postimg.cc/9XKbHM0Y/image.png'
authors: ["Joaquin Decima"]
download: "https://www.mediafire.com/file/z94glxyd03pqffx/Manual_ba%25CC%2581sico_de_Orientacio%25CC%2581n.pdf/file"
---

## EDITA

* Scouts de España - 2017


## COLABORAN


* Ana Pérez Curiel - Exploradores de Madrid
* Equipo de Medio Ambiente de Scouts de España
* Fernándo González - Scouts de Galicia
* Héctor Ruiz - Scouts de La Rioja
* Iris Cervantes - Exploradores de Madrid Alfonso Sevilla -
* Scouts Valencians
* Alejandro Luipiani - Scouts de Andalucía
* Marta Mora - Scouts de Melilla
* Aranzazu Pinilla - Souts de Aragón


## REVISIÓN


* Servicio de Educación Scout bajo la coordinación de David Navalón Mateos.
* Oscar Calderón. Técnico de Comunicación de Scouts de España.


## DISEÑO Y MAQUETACIÓN

* Eva Aro


## FOTOGRAFÍAS


* Scouts de España


## LICENCIA

Reconocimiento - NoComercial (by-nc): Se permite la generación de obras derivadas siempre que no se haga un uso comercial. Tampoco se puede utilizar la obra original con finalidades comerciales.

## Indice

1. INTRODUCCIÓN AL MANUAL Pg.6
2. CONCEPTOS GENERALES Pg.9
3. ORIENTACIÓN POR ELEMENTOS NATURALES Pg.12
    * ESTRELLA
    * LUNA
    * SOL
4. BRÚJULA Pg.24
5. LA MARCHA DUFOUR Pg.28
6. COMO TRABAJAR LA ORIENTACIÓN EN LAS SECCIONES.
    * CASTORES Pg.40
    * MANADA Pg.44
    * TROPA Pg.54
    * ESCULTA Pg.75
    * CLAN Pg.80
7. BIOGRAFÍA Pg.82
    * TUTORIAL IBERPIX: Cómo diseñar el mapa de una ruta Pg.84
    * GEOCACHING Pg.94
