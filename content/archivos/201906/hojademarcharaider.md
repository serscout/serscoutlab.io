---
title: 'Hoja de Marcha Raider'
date: '2019-06-24'
description: 'Estos dias me encuentro trabajando en Scout de Argentina y me di el permiso de editar esto para que sea "Mas Scout".'
type: 'archivos'
categories: ["raiders"]
theme: "progresion"
tags: ["raider", "caminantes", "hoja", "marcha", "progresion"]
color: "raider"
img: 'https://i.postimg.cc/85FyG6Gp/hojamarcha.png'
authors: ["Joaquin Decima"]
download: "https://docs.google.com/document/d/1zehsFOJdGBG1pPBbDXVmcoczV0EfQshWPZS5Z4drH7o/edit?usp=sharing"
---

En Scout de argentina los Raiders (o caminantes como los llama dicha asociación) tienen una hoja de marcha, es una especie de guía que los jóvenes deben seguir para su progresión.

{{< img src="https://i.postimg.cc/85FyG6Gp/hojamarcha.png" >}}

Durante mi adaptación en dicha asociación decidí modificar un poco esta "Hoja de Marcha" con el fin de obtener algo un poco más Scout. Alejado de cualquier mancha de dicha asociación...

Espero que sea útil...
