---
title: 'Manual de Acampe'
date: '2019-06-20'
description: 'Hoy traemos un manual de acampe o “handbook” de acampe que cuenta con la información mínima que debemos transferir a nuestros jóvenes.'
type: 'archivos'
categories: ["tropa","raiders"]
color: "universal"
theme: "campamento"
tags: ["handbook", "manual", "acampe", "campamento"]
img: 'https://i.postimg.cc/Z5GGFtYm/tapa.png'
authors: ["Joaquin Decima"]
download: "https://www.mediafire.com/file/t33juaxip662pvr/Manual_Acampe.pdf/file"
---
El siguiente manual lo encontré a medida que navegaba por internet. El mismo contiene un panorama básico de los conceptos mínimos que debe tener un scout para acampar. Desde mi punto de vista está más orientado a la Tropa y a los Raides dado que es donde más deben aprender estas técnicas.

{{< img src="https://i.postimg.cc/RZJrHVBG/indice.png" >}}

El manual está pensado para dar un panorama de lo básico, a esto debemos sumarle muchos temas que convertirán al joven en un experto en acampe.
