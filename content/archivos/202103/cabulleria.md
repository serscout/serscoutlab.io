---
title: 'Manual de Cabulleria (ASDE)'
date: '2021-03-19'
description: 'Hoy traemos un manual de cabulleria o “handbook” de nudos que cuenta con la información mínima que debemos transferir a nuestros jóvenes.'
type: 'archivos'
categories: ["tropa","raiders","manada","rovers"]
color: "universal"
theme: "campamento"
tags: ["cabulleria", "nudos", "campamento", "supervivencia", "sogas","asde","españa"]
img: 'https://i.postimg.cc/jq6PQpYt/image.png'
authors: ["Joaquin Decima"]
download: "https://www.mediafire.com/file/18spfyrqr82v8ui/Manual_Cabuyeri%25CC%2581a_interactivo.pdf/file"
---

En esta oportunidad traemos un manual que facilita ASDE que si bien se presenta como un manual básico es bastante completo y es muy útil tanto para jóvenes como para educadores.

{{< img src="https://i.postimg.cc/jq6PQpYt/image.png" >}}

## CRÉDITOS:

* EDITA: ASDE SCOUTS DE ESPAÑA.
* COLABORAN:
  * Coordinador: Equipo Educación Ambiental y para el Consumo responsable: Javier Atiénzar
  * Martínez. ASDE Scouts de España
  * Sheila Vilches Crespo. ASDE Scouts de Andalucía.
  * René Díaz González. Exploradores del Principado de Asturias.
  * María Rojas Estévez. ASDE Scouts de Canarias.
  * Jesús Vega Rossell. ASDE Scouts de Extremadura.
  * Raúl Fuentes Solís. ASDE Scouts Extremadura.
  * Fernando González. Muiño. ASDE Scouts de Galicia.
  * Héctor Ruíz Ortega. ASDE Scouts de La Rioja.
  * Tomás de Garay Herrera. ASDE Exploradores de Madrid.
  * Marta Mora Navarro. ASDE Scouts Melilla.
  * Alfonso Sevilla Prieto. ASDE Scouts Valencians.


## REVISIÓN:

Servicio de Educación Scout bajo la coordinación de David Navalón Mateos y Equipo técnico
de ASDE.
