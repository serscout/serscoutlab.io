---
title: 'Fuegos'
date: '2019-09-16'
description: 'Hoy traemos un folleto sobre los fuegos más básicos e importantes que deben saber todos los scouts.'
type: 'archivos'
categories: ["tropa","raiders","rovers"]
color: "universal"
theme: "campamento"
tags: ["fuegos", "basicos", "supervivencia", "campamento"]
img: 'https://i.postimg.cc/hGNWFQY2/tipos-de-fuegos.png'
authors: ["Joaquin Decima"]
download: "https://www.mediafire.com/file/l80n71na62c0i05/Tipo_de_Fuegos.pdf/file"
---

Los scouts tenemos que tener un nivel técnico casi impecable. Esto se debe realizar en varias disciplinas y una de ella son los fuegos. Como scouts tenemos que saber los diferentes tipos de fuegos y sus funcionalidades.

{{< img src="https://i.postimg.cc/hGNWFQY2/tipos-de-fuegos.png" >}}

En esta ocasión tenemos la suerte que la gente de acción católica los busco y realizo un resumen muy fiel y con mucha información útil para que los jóvenes y para que puedan iniciarse en los fuegos.
