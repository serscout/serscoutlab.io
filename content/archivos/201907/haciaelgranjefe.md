---
title: 'Hacia el Gran Jefe'
date: '2019-07-01'
description: 'Un hermoso libro que todo Scout católico debe tener a mano'
type: 'archivos'
categories: ["raiders","rovers","manada","tropa"]
color: "universal"
theme: "biblioteca"
tags: ["gran", "jefe", "hacia", "oraciones", "campamento", "biblioteca"]
img: 'https://i.postimg.cc/8z86Tbgt/hacia-el-gran-jefe.png'
authors: ["Joaquin Decima"]
download: "https://www.mediafire.com/file/717f9xd86g1ch6g/hacia_el_gran_jefe.pdf/file"
---

Este hermoso libro me acompaño durante mi juventud en el cristianismo y dentro del movimiento. si bien hoy en día no soy cristiano se lo recomiendo fuertemente a mis jóvenes y creo que esta muy bueno tenerlo a mano.

{{< img src="https://i.postimg.cc/4yqh7674/hegj1.png" >}}
{{< img src="https://i.postimg.cc/SNY90v3n/hegj2.png" >}}
{{< img src="https://i.postimg.cc/MpyjHjWx/hegj3.png" >}}
{{< img src="https://i.postimg.cc/nVPmPJR8/hegj4.png" >}}

¡Espero que les sea útil para su día a día y los campamentos!

Salvar!
