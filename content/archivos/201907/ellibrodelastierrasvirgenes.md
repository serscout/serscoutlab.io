---
title: 'El libro de las Tierras Virgenes'
date: '2019-07-05'
description: 'Hoy traemos un libro para nuestra rama menor, tal vez el libro mas importante para ellos'
type: 'archivos'
categories: ["manada"]
color: "manada"
theme: "historias"
tags: ["historias", "libro", "tierras", "virgenes", "manada"]
img: 'https://onemorelibrary.com/components/com_djclassifieds/images/item/1311_el_libro_de_las_tierras_virgenes_-_rudyard_kipling_thb.jpg'
authors: ["Joaquin Decima"]
download: "https://www.mediafire.com/file/5yw5w95ih1bfbb4/El_Libro_de_las_Tierras_V%EDrgenes.pdf/file"
---


Como todos sabemos la manada está inspirada básicamente en este gran libro, toda su mistica está inspirada en el mismo.

{{< img src="https://onemorelibrary.com/components/com_djclassifieds/images/item/1311_el_libro_de_las_tierras_virgenes_-_rudyard_kipling_thb.jpg" >}}
